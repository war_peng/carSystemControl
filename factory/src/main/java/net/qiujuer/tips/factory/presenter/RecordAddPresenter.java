package net.qiujuer.tips.factory.presenter;

import android.util.Log;

import net.qiujuer.genius.res.Resource;
import net.qiujuer.tips.factory.cache.Cache;
import net.qiujuer.tips.factory.model.Model;
import net.qiujuer.tips.factory.model.api.RecordTransModel;
import net.qiujuer.tips.factory.model.api.SyncPushModel;
import net.qiujuer.tips.factory.model.api.SyncPushRspModel;
import net.qiujuer.tips.factory.model.db.HealthModel;
import net.qiujuer.tips.factory.model.db.ModelStatus;
import net.qiujuer.tips.factory.model.db.RecordModel;
import net.qiujuer.tips.factory.model.xml.AccountPreference;
import net.qiujuer.tips.factory.model.xml.DailyModel;
import net.qiujuer.tips.factory.util.TipsCalender;
import net.qiujuer.tips.factory.view.RecordAddView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;


public class RecordAddPresenter {
    private RecordAddView mView;
    final SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
    private RecordModel mModel;
    private SyncPushModel syncPushModel;
    public RecordAddPresenter(RecordAddView view) {
        mView = view;
    }

    public static void dailyCreate() {
        DailyModel dailymodel = new DailyModel();
        TipsCalender calender = TipsCalender.getNow();
        TipsCalender lastcalender = dailymodel.getDailyAddDate();
        Log.d("RecordAddPresenter","calendernow = "+ calender+"last = "+ lastcalender);
        if (lastcalender.toLong() == calender.toLong()) {
            return;
        }
        int perioddays = -lastcalender.distanceNow();
        if(perioddays > 30) {
            return;
        }
        for (int i = perioddays; i > 0; i--) {
            calender.AddDays(-i, calender);
            createdailyrecord(calender);
        }

        dailymodel.setDailyAddDate(TipsCalender.getNow());

        dailymodel.save();

    }

    public static void createdailyrecord(TipsCalender calender) {
        List<HealthModel> models = HealthModel.getAllPeriodMode();
        for (HealthModel healthmodel : models) {
            RecordModel model = new RecordModel();
            model.setMark(UUID.randomUUID());
            model.setHealthname(healthmodel.getName());
            model.setHealthvalue(healthmodel.getPeriodvalue());
            model.setType(healthmodel.getHealthtype());
            //  model.setBrief(mView.getBrief());
            if (healthmodel.getHealthtype() == 1) {
                model.setColor(Resource.Color.COLORS[2]);//red);
            } else {
                model.setColor(Resource.Color.COLORS[11]);//green);
            }

            model.setDate(calender);
            //    model.setContact(mView.getContact());
            try {
                //clare 存储到数据库
                model.save();
                model.updateMonthValue(calender.toLong(), healthmodel.getPeriodvalue());
                //加入缓存，避免每次操作数据库造成卡顿
                Cache.getInstance().add(model);
                //       mView.setStatus(model.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public RecordModel createQuickRecord(HealthModel healthmodel) {
        RecordModel model = new RecordModel();
        long id = model.getRecordfromDay(TipsCalender.getNow().toLong(), healthmodel.getName());
        if (id != -1) {//同类型当天已存在 基础上增加 不再分开
            model = RecordModel.get(id);
            model.setHealthvalue(model.getHealthvalue() +
                    healthmodel.getQuickvalue()* (int) healthmodel.getPerValue());
            model.setStatus(ModelStatus.STATUS_EDIT);
            model.save();
            Cache.getInstance().edit(model);
        } else {
            model.setMark(UUID.randomUUID());
            model.setHealthname(healthmodel.getName());
            model.setHealthvalue(healthmodel.getQuickvalue() * (int) healthmodel.getPerValue());
            model.setType(healthmodel.getHealthtype());
            if (healthmodel.getHealthtype() == 1) {
                model.setColor(Resource.Color.COLORS[2]);//red);
            } else {
                model.setColor(Resource.Color.COLORS[11]);//green);
            }
            model.setDate(TipsCalender.getNow());
            //     model.setContact(mView.getContact());
            try {
                //clare 存储到数据库
                model.save();
                model.updateMonthValue(model.getDate(), model.getHealthvalue());
                //加入缓存，避免每次操作数据库造成卡顿
                Cache.getInstance().add(model);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return model;
    }
    public void DeleteQuickValue(HealthModel model) {
        model.setQuickvalue(0);
        try {
            //clare 存储到数据库
            model.save();
            Cache.getInstance().deleteQuick(model);
            mView.setStatus(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateQuickValue(HealthModel model) {
        model.setQuickvalue(mView.getHealthvalue() / mView.getHealthPerValue());
        try {
            //clare 存储到数据库
            model.save();
            Cache.getInstance().addQuick(model);
            mView.setStatus(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editQuickValue(HealthModel model) {
        model.setQuickvalue(mView.getHealthvalue() / mView.getHealthPerValue());
        try {
            //clare 存储到数据库
            model.save();
            Cache.getInstance().editQuick(model);
            mView.setStatus(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void create() {

        RecordModel model = new RecordModel();
        long id = model.getRecordfromDay(mView.getDate().toLong(), mView.getHealthname());
        if (id != -1) {//同类型当天已存在 基础上增加 不再分开
            mModel = RecordModel.get(id);
            mModel.setHealthvalue(mModel.getHealthvalue() + mView.getHealthvalue());
            mModel.save();
            Cache.getInstance().edit(mModel);
            mView.setStatus(id);
        } else {
            model.setMark(UUID.randomUUID());
            model.setHealthname(mView.getHealthname());
            model.setHealthvalue(mView.getHealthvalue());
            model.setType(mView.getType());
            model.setBrief(mView.getBrief());
            if (mView.getType() == 1) {
                model.setColor(Resource.Color.COLORS[2]);//red);
            } else {
                model.setColor(Resource.Color.COLORS[11]);//green);
            }
            int nowcombo = model.getRecordLastDayCombo(TipsCalender.getLastDay().toLong(), mView.getHealthname());
            model.setCombo(nowcombo + 1);

            model.setDate(mView.getDate());
            model.setHealth(mView.getHealth());
            model.setContact(mView.getContact());

            try {
                //clare 存储到数据库
                model.save();
                model.updateMonthValue(mView.getDate().toLong(), mView.getHealthvalue());
                //加入缓存，避免每次操作数据库造成卡顿
                Cache.getInstance().add(model);
                mView.setStatus(model.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String GetSyncRecordjson() {
        syncPushModel = SyncPushModel.build();
        if (syncPushModel == null) {
            return "";
        }

        JSONObject json = syncPushModel.toJson();
        if (json == null)
            return "";
        log("SyncPush json: " + json.toString());
        return  json.toString();
    }
    private static void log(String msg) {
        if (Model.DEBUG)
            Log.e("clare", msg);
    }


}
