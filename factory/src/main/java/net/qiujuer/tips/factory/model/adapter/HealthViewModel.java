package net.qiujuer.tips.factory.model.adapter;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import net.qiujuer.genius.res.Resource;
import net.qiujuer.tips.factory.model.Model;
import net.qiujuer.tips.factory.model.db.HealthModel;

import java.util.UUID;

/**
 * Provide data for Adapter
 */
public class  HealthViewModel implements Comparable<HealthViewModel>, Parcelable {
    private UUID id = Model.EMPTY_ID;
    private int color = Color.TRANSPARENT;
    private String name = "";
    private int quickvalue = 0;
    private int relation = 1;
    private String unit = "";
    public HealthViewModel() {

    }

    public void set(HealthModel model) {
        id = (model.getMark());
        name = (model.getName());
        quickvalue = (model.getQuickvalue());
        unit = (model.getUnit());
        relation = model.getRelation();
        color = getNameColor(name);
    }

    public UUID getId() {
        return id;
    }

    public int getColor() {
        return color;
    }


    public void setColor(int color) {
        this.color = color;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }
    public void setQuickvalue(int value) {
        this.quickvalue = value;
    }

    public int getQuickvalue() {
        return quickvalue;
    }
    @Override
    public int compareTo(HealthViewModel another) {
        int lhs = this.relation;
        int rhs = another.relation;

        if (lhs == rhs) {
            return 0;
        } else if (lhs < rhs) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        long mostSigBits = id.getMostSignificantBits();
        long leastSigBits = id.getLeastSignificantBits();

        dest.writeLong(mostSigBits);
        dest.writeLong(leastSigBits);

        dest.writeInt(relation);
        dest.writeString(name);
        dest.writeInt(quickvalue);
        dest.writeString(unit);
        dest.writeInt(color);
    }

    protected HealthViewModel(Parcel in) {
        long mostSigBits = in.readLong();
        long leastSigBits = in.readLong();

        id = new UUID(mostSigBits, leastSigBits);

        relation = in.readInt();
        name = in.readString();
        quickvalue = in.readInt();
        unit = in.readString();
        color = in.readInt();
    }

    public static final Creator<HealthViewModel> CREATOR = new Creator<HealthViewModel>() {
        @Override
        public HealthViewModel createFromParcel(Parcel in) {
            return new HealthViewModel(in);
        }

        @Override
        public HealthViewModel[] newArray(int size) {
            return new HealthViewModel[size];
        }
    };

    /**
     * 返回联系人姓名的图标背景色
     *
     * @param name 联系人的姓名
     */
    public static int getNameColor(String name) {
        int bgColor = Color.TRANSPARENT;
        int[] colorArray = Resource.Color.COLORS;
        int index = 0;
        if (!TextUtils.isEmpty(name))
            index = Math.abs(name.hashCode()) % (colorArray.length);
        if (index < colorArray.length)
            bgColor = colorArray[index];
        return bgColor;
    }
}
