package net.qiujuer.tips.factory.view;

import net.qiujuer.tips.factory.adapter.BaseAdapter;
import net.qiujuer.tips.factory.model.adapter.HealthViewModel;


public interface HealthView extends BaseAdapter<HealthViewModel> {
    void setLoading(boolean isLoad);
}
