package net.qiujuer.tips.factory.presenter;

import net.qiujuer.tips.factory.model.db.ContactModel;
import net.qiujuer.tips.factory.model.db.HealthModel;
import net.qiujuer.tips.factory.view.ContactDetailView;
import net.qiujuer.tips.factory.view.HealthDetailView;


public class HealthDetailPresenter {
    private HealthDetailView mView;
    private HealthModel mModel;

    public HealthDetailPresenter(HealthDetailView view) {
        mView = view;
    }

    public boolean refresh() {
        mModel = HealthModel.get(mView.getId());
        if (mModel != null) {
            mView.setNameStr(mModel.getName());
         /*   mView.setPhoneNumber(mModel.getPhone());
            mView.setQQ(mModel.getQQNumber());
            mView.setSex(mModel.getSex());*/
            mView.setRelation(mModel.getRelation());
            mView.setColor(mModel.getColor());
            return true;
        }
        return false;
    }

}
