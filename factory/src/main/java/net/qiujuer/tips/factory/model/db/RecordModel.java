/**
 * CREATE TABLE Record
 * (
 * Id INTEGER PRIMARY KEY AUTOINCREMENT,
 * Brief TEXT,
 * Changed INTEGER,
 * Color INTEGER,
 * Contacts INTEGER REFERENCES Contact(Id) ON DELETE NO ACTION ON UPDATE NO ACTION,
 * Created INTEGER,
 * Date INTEGER,
 * Last INTEGER,
 * Mark TEXT,
 * Status INTEGER,
 * Type INTEGER
 * )
 */
package net.qiujuer.tips.factory.model.db;


import android.database.Cursor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.CursorResult;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.raizlabs.android.dbflow.sql.language.property.IProperty;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.qiujuer.tips.factory.model.xml.DailyModel;
import net.qiujuer.tips.factory.util.TipsCalender;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.raizlabs.android.dbflow.sql.language.Method.sum;

@Table(database = AppDatabase.class, name = "Record")
public class RecordModel extends BaseModel implements ModelStatus {
    public final static int TYPE_BIRTHDAY = 1;
    public final static int TYPE_MEMORIAL = 2;
    public final static int TYPE_FUTURE = 3;
    public static RecordModel instance;

    static {
        instance = new RecordModel();
    }
    /*一刻钟为最短能量周期 白天工作每小时消耗4个能量 晚上睡觉每小时恢复2个能量
     * 故每天正常消耗能量为16*4-16 = 32 其他项目额为计算
     * */
    public static int PERDAYREDUCE = 32;
    public static RecordModel getInstance() {
        return instance;
    }
    public RecordModel() {
        super();
        mark = UUID.randomUUID();
        date = 201501010;
        changedTime = System.currentTimeMillis();
        create = new Date(changedTime);
        last = create;
        status = STATUS_ADD;
    }

    @PrimaryKey(autoincrement = true)
    @Column(name = "Id")
    @Expose
    private long id;

    /**
     * 标识
     */
    @SerializedName("Id")
    @Column(name = "Mark", typeConverter = UUIDConverter.class)
    private UUID mark;

    @SerializedName("healthname")
    @Column(name = "healthname")
    private String healthname;

    @SerializedName("healthvalue")
    @Column(name = "healthvalue")
    private int healthvalue;

    @SerializedName("Brief")
    @Column(name = "Brief")
    private String brief;

    //Type 损耗还是补充
    @SerializedName("Type")
    @Column(name = "Type")
    private int type;

    @SerializedName("Color")
    @Column(name = "Color")
    private int color;

    @SerializedName("Combo")
    @Column(name = "Combo")
    private int combo;

    @SerializedName("Date")
    @Column(name = "Date")
    private long date;

    @SerializedName("Create")
    @Column(name = "Created")
    private Date create;


    @SerializedName("Last")
    @Column(name = "Last")
    private Date last;

    @ForeignKey()
    //@ForeignKeyReference(columnName = "Contact", columnType = , foreignKeyColumnName = )
    private HealthModel health;

    @ForeignKey()
    //@ForeignKeyReference(columnName = "Contact", columnType = , foreignKeyColumnName = )
    private ContactModel contact;

    @Expose
    @Column(name = "Status")
    private int status;

    @Expose
    @Column(name = "ChangedTime")
    private long changedTime;

    @Expose
    private transient TipsCalender mDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UUID getMark() {
        return mark;
    }

    public void setMark(UUID mark) {
        this.mark = mark;
    }

    public String getHealthname() {
        return healthname;
    }

    public void setHealthname(String healthname) {
        this.healthname = healthname;
    }

    public int getHealthvalue() {
        return healthvalue;
    }

    public void setHealthvalue(int healthvalue) {
        this.healthvalue = healthvalue;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getCombo() {
        return combo;
    }

    public void setCombo(int combo) {
        this.combo = combo;
    }

    public TipsCalender getDateCalender() {
        if (this.mDate == null)
            this.mDate = new TipsCalender(date);
        else
            this.mDate.set(date);
        return this.mDate;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
        if (this.mDate != null)
            this.mDate.set(date);
    }

    public void setDate(TipsCalender date) {
        this.mDate = date;
        this.date = date.toLong();
    }

    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }

    public Date getLast() {
        return last;
    }

    public HealthModel getHealth() {
        return health;
    }

    public void setHealth(HealthModel health) {
        this.health = health;
    }

    public ContactModel getContact() {
        return contact;
    }

    public void setContact(ContactModel contact) {
        this.contact = contact;
    }

    public long getChangedTime() {
        return changedTime;
    }

    public void setChangedTime(long changedTime) {
        this.changedTime = changedTime;
    }

    public void setLast(Date last) {
        this.last = last;
        this.changedTime = last.getTime();
    }

    public void setStatus(int status) {
        this.status = status;
        this.changedTime = System.currentTimeMillis();
    }

    public int getStatus() {
        return status;
    }

    public boolean isDelete() {
        return status == STATUS_DELETE;
    }

    public boolean isEdit() {
        return status == STATUS_EDIT;
    }

    public boolean isAdd() {
        return status == STATUS_ADD;
    }

    public boolean isSynced() {
        return (status == STATUS_UPLOADED);
    }

    public static List<RecordModel> getAll() {
        // Status <> STATUS_DELETE
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_DELETE))
                .queryList();
    }

    public static List<RecordModel> getAllChanged(long date) {
        // ChangedTime > date
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.ChangedTime.greaterThan(date))
                .queryList();
    }

    public static List<RecordModel> getAllYearData(long fromdate,long todate) {
        // Status <> STATUS_DELETE
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_DELETE),
                        RecordModel_Table.Date.greaterThan(fromdate),
                        RecordModel_Table.Date.lessThan(todate))
                .orderBy(RecordModel_Table.Date, false)
                .queryList();
    }


    public static List<RecordModel> getAllUnSync() {
        // Status <> STATUS_DELETE
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_UPLOADED))
                .queryList();
    }

    public static RecordModel get(UUID id) {
        // Mark = ?
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Mark.is(id))
                .querySingle();
    }

    public static RecordModel get(String id) {
        return get(UUID.fromString(id));
    }

    public static RecordModel get(long id) {
        // Id = ?
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Id.is(id))
                .querySingle();
    }


    public static long getCount(int type) {
        // Status <> ? and Type = ?
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_DELETE))
                .and(RecordModel_Table.Type.is(type))
                .queryList().size();
    }

    public static void updateMonthValue(long date, int value) {
        DailyModel dailymodel = new DailyModel();
        dailymodel.setChangeDate(date);
        dailymodel.setChangevalue(value);
        dailymodel.save();
    }
    public static int getRecordtotal() {
        // Status <> ? orderBy Changed DESC limit count
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;

    }
    //截止到某日期前的元精值
    public static int getRecordtotal(long todate) {
        // Status <> ? orderBy Changed DESC limit count
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.Date.lessThan(todate),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;

    }
    public static int getRecordtotalfromname(String name) {
        // Status <> ? orderBy Changed DESC limit count
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.healthname.is(name),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;

    }

    public static int getRecordvaluefromday(int type, long firstdate, long lastdate) {
        // Status <> ? orderBy Changed DESC limit count
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.Date.between(firstdate).and(lastdate),
                        RecordModel_Table.Type.is(type),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;

    }

    public static long getRecordSumByDay(long Date) {
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.Date.is(Date),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;
    }

    public static long getRecordDaysSum(String name) {
        int sum = 0;
        Cursor cursor = SQLite.select(sum(RecordModel_Table.healthvalue))
                .from(RecordModel.class)
                .where(RecordModel_Table.healthname.is(name),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .query();

        if (cursor.moveToNext()) {
            sum = cursor.getInt(0);
        }
        return sum;
    }

    public static long getRecordfromDay(long Date, String name) {
        // Status <> ? orderBy Changed DESC limit count
        List<RecordModel> dataModels = SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Date.is(Date), RecordModel_Table.healthname.is(name),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .queryList();
        if (dataModels.size() > 0)
            return dataModels.get(0).getId();
        else
            return -1;
    }

    public static int getRecordLastDayCombo(long Date, String name) {
        // Status <> ? orderBy Changed DESC limit count
        List<RecordModel> dataModels = SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Date.is(Date), RecordModel_Table.healthname.is(name),
                        RecordModel_Table.Status.isNot(STATUS_DELETE))
                .queryList();
        if (dataModels.size() > 0)
            return dataModels.get(0).getCombo();
        else
            return 0;
    }

    public static List<RecordModel> getNewest(int count) {
        // Status <> ? orderBy Changed DESC limit count
        return SQLite.select()
                .from(RecordModel.class)
                .where(RecordModel_Table.Status.isNot(STATUS_DELETE))
                .orderBy(RecordModel_Table.ChangedTime, false)
                .limit(count)
                .queryList();
    }

    @Override
    public String toString() {
        return "RecordModel{" +
                "id=" + id +
                ", mark=" + mark +
                ", brief='" + brief + '\'' +
                ", type=" + type +
                ", color=" + color +
                ", date=" + date +
                ", create=" + create +
                ", last=" + last +
                ", contact=" + contact +
                ", status=" + status +
                ", changedTime=" + changedTime +
                ", mDate=" + mDate +
                '}';
    }

}
