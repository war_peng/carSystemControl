package net.qiujuer.tips.factory.cache;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import net.qiujuer.tips.factory.model.Model;
import net.qiujuer.tips.factory.model.adapter.ContactViewModel;
import net.qiujuer.tips.factory.model.adapter.HealthViewModel;
import net.qiujuer.tips.factory.model.adapter.NewestModel;
import net.qiujuer.tips.factory.model.adapter.RecordViewModel;
import net.qiujuer.tips.factory.model.db.ContactModel;
import net.qiujuer.tips.factory.model.db.HealthModel;
import net.qiujuer.tips.factory.model.db.RecordModel;
import net.qiujuer.tips.factory.model.db.UserDataModel;
import net.qiujuer.tips.factory.presenter.AppPresenter;
import net.qiujuer.tips.factory.service.IMissServiceInterface;
import net.qiujuer.tips.factory.service.MissService;
import net.qiujuer.tips.factory.service.bean.MissServiceBean;

import java.util.List;

public class Cache extends BroadcastReceiver {
    private static boolean isDispose = true;
    private final static Cache instance;

    static {
        instance = new Cache();
    }

    public static void destroy() {
        isDispose = true;
        try {
            Model.getApplication().unregisterReceiver(instance);
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance.cacheNotify.clear();
    }

    public static void init() {
        isDispose = false;

        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(MissService.ACTION_MISS_MAIN);
            Model.getApplication().registerReceiver(instance, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        instance.initData();
    }

    public static Cache getInstance() {
        return instance;
    }

    public final CacheNotify cacheNotify = new CacheNotify();
    private final CacheStaCount cacheStaCount = new CacheStaCount();

    public CacheStaCount getCacheStaCount() {
        return cacheStaCount;
    }

    public List<RecordViewModel> getRecords() {
        try {
            IMissServiceInterface service = AppPresenter.getService();
            if (service == null)
                return null;
            MissServiceBean bean = service.getMissBean();
            if (bean == null)
                return null;
            return bean.getTimeLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<HealthViewModel> getHealths(int type) {
        try {
            IMissServiceInterface service = AppPresenter.getService();
            if (service == null)
                return null;
            MissServiceBean bean = service.getMissBean();
            if (bean == null)
                return null;
            if(type == 1){
                return bean.getHealths();
            }else if(type == 2){
                return bean.getLosses();
            }else if(type == 3){
                return bean.getQuickes();
            }else{
                return bean.getHealths();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ContactViewModel> getContacts() {
        try {
            IMissServiceInterface service = AppPresenter.getService();
            if (service == null)
                return null;
            MissServiceBean bean = service.getMissBean();
            if (bean == null)
                return null;
            return bean.getContacts();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<NewestModel> getNewest() {
        try {
            IMissServiceInterface service = AppPresenter.getService();
            if (service == null)
                return null;
            MissServiceBean bean = service.getMissBean();
            if (bean == null)
                return null;
            return bean.getNewest();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (MissService.ACTION_MISS_MAIN.equals(action)) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        IMissServiceInterface service = AppPresenter.getService();
                        if (service == null)
                            return;

                        MissServiceBean bean = service.getMissBean();
                        if (bean == null)
                            return;


                        cacheNotify.notifyRecordsList(bean.getTimeLine());
                        cacheStaCount.set(bean);
                        cacheNotify.notifySTTT(cacheStaCount);
                        cacheNotify.notifyNewest(bean.getNewest());
                        cacheNotify.notifyContacts(bean.getContacts());
                        cacheNotify.notifyHealths(bean.getHealths());
                        cacheNotify.notifyQuickHealths(bean.getQuickes());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            Model.getThreadPool().execute(runnable);
        }
    }

    private void initData() {
        try {
            IMissServiceInterface service = AppPresenter.getService();
            if (service == null)
                return;
      //      service.order();
            MissServiceBean bean = service.getMissBean();
            if (bean == null)
                return;
            cacheStaCount.set(bean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void add(UserDataModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().add(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void add(RecordModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().add(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 /*   public void add(HealthModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().addHealth(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    public void delete(RecordModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().delete(model.getMark().toString(), model.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void edit(RecordModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().edit(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void add(ContactModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().addContact(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(ContactModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().deleteContact(model.getMark().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void edit(ContactModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().editContact(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void edit(HealthModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().editHealth(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addQuick(HealthModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().AddQuickHealth(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void editQuick(HealthModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().editQuickHealth(model.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteQuick(HealthModel model) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().DelQuickHealth(model.getMark().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void refreshDesktop(int day) {
        if (isDispose)
            return;

        try {
            AppPresenter.getService().refreshDesktop(day);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
