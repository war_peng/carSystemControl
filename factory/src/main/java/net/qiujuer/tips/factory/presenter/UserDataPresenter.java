package net.qiujuer.tips.factory.presenter;

import android.content.SharedPreferences;

import net.qiujuer.tips.factory.cache.Cache;
import net.qiujuer.tips.factory.model.db.RecordModel;
import net.qiujuer.tips.factory.model.db.UserDataModel;
import net.qiujuer.tips.factory.view.RecordAddView;

import java.util.UUID;


public class UserDataPresenter {
    private RecordAddView mView;

    public UserDataPresenter() {

    }

    public void create(long monthdate, int change) {
        UserDataModel model = new UserDataModel();

        model.setMonthDate(monthdate);
        model.setMonthSum(model.getMonthValue(monthdate) + change);
        if (change < 0) {
            model.setMonthReduce(model.getMonthReduceValue(monthdate) + change);
        }

//          model.setMonthReduce(reduce);
        try {
            //clare 存储到数据库
            model.save();
            //加入缓存，避免每次操作数据库造成卡顿
//            Cache.getInstance().add(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
