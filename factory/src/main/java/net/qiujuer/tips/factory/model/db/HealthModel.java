/**
 * CREATE TABLE Contact
 * (
 * Id INTEGER PRIMARY KEY AUTOINCREMENT,
 * QQ TEXT,
 * Gender INTEGER,
 * Mark TEXT,
 * Name TEXT,
 * ValueDisc TEXT,
 * Relation INTEGER,
 * Status INTEGER,
 * Changed INTEGER
 * )
 */
package net.qiujuer.tips.factory.model.db;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.qiujuer.tips.factory.model.adapter.ContactViewModel;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@ModelContainer
@Table(database = AppDatabase.class, name = "HealthModel")
public class HealthModel extends BaseModel implements ModelStatus {
    public final static int GENDER_MAN = 1;
    public final static int GENDER_WOMAN = 0;

    public HealthModel() {
        super();
        status = STATUS_ADD;
    }

    @PrimaryKey(autoincrement = true)
    @Column(name = "Id")
    @Expose
    private long id;

    /**
     * 标识
     */
    @SerializedName("Id")
    @Column(name = "Mark", typeConverter = UUIDConverter.class)
    private UUID mark;
    //养生类型名称
    @SerializedName("Name")
    @Column(name = "Name")
    private String name;
    
    //单位时间（一刻钟）  消耗or增加的能量值
    @SerializedName("PerValue")
    @Column(name = "PerValue")
    private float PerValue;

    //单位名称 分钟  小时
    @SerializedName("Unit")
    @Column(name = "Unit")
    private String Unit;

    //类型 good or bad
    @SerializedName("Healthtype")
    @Column(name = "Healthtype")
    private int healthtype;
    

    //快速记录 能量值
    @SerializedName("Quickvalue")
    @Column(name = "Quickvalue")
    private int quickvalue;

    //周期记录 能量值
    @SerializedName("Periodvalue")
    @Column(name = "Periodvalue")
    private int periodvalue;
    //排序后的顺序
    @SerializedName("Order")
    @Column(name = "Order")
    private int Order;

    @SerializedName("Relation")
    @Column(name = "Relation")
    private int relation;

    @Expose
    @Column(name = "Status")
    private int status;

    @Expose
    List<RecordModel> recordModels;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UUID getMark() {
        return mark;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public boolean isDelete() {
        return status == STATUS_DELETE;
    }

    public boolean isEdit() {
        return status == STATUS_EDIT;
    }

    public boolean isAdd() {
        return status == STATUS_ADD;
    }

    public boolean isSynced() {
        return (status == STATUS_UPLOADED);
    }

    public String getName() {
        return name;
    }

    public void setMark(UUID id) {
        this.mark = id;
    }

    public void setName(String name) {
        this.name = name;
    }



    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }

    public int getColor() {
        return ContactViewModel.getNameColor(name);
    }



    public float getPerValue() {
        return PerValue;
    }

    public void setPerValue(float perValue) {
        PerValue = perValue;
    }

    public int getHealthtype() {
        return healthtype;
    }

    public void setHealthtype(int healthtype) {
        this.healthtype = healthtype;
    }

    public int getQuickvalue() {
        return quickvalue;
    }

    public void setQuickvalue(int quickvalue) {
        this.quickvalue = quickvalue;
    }

    public int getPeriodvalue() {
        return periodvalue;
    }

    public void setPeriodvalue(int periodvalue) {
        this.periodvalue = periodvalue;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public static void deleteAll() {
        // Mark = ?

        Delete.table(HealthModel.class);

    }
    public static HealthModel get(UUID id) {
        // Mark = ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Mark.is(id))
                .querySingle();
    }
    public static HealthModel getfromorder(int id,int type) {
        //Id = ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Order.is(id))
                .and(HealthModel_Table.Healthtype.is(type))
                .querySingle();
    }
    public static HealthModel get(long id) {
        //Id = ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Id.is(id))
                .querySingle();
    }

    public static HealthModel get(String name) {
        //Id = ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Name.is(name))
                .querySingle();
    }

    public static List<HealthModel> getperiodmode() {
        // Status <> ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Status.isNot(STATUS_DELETE))
                .queryList();
    }

    public static List<HealthModel> getAllQuickMode() {
        // Status <> ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Quickvalue.isNot(0))
                .queryList();
    }

    public static List<HealthModel> getAllPeriodMode() {
        // Status <> ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Periodvalue.isNot(0))
                .orderBy(HealthModel_Table.Order, false)
                .queryList();
    }

    public static List<HealthModel> getAll() {
        // Status <> ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Status.isNot(STATUS_DELETE))
                .queryList();
    }

    public static List<HealthModel> getAllUnSync() {
        // Status <> ?
        return SQLite.select()
                .from(HealthModel.class)
                .where(HealthModel_Table.Status.isNot(STATUS_UPLOADED))
                .queryList();
    }


    //@OneToMany(methods = {OneToMany.Method.ALL}, variableName = "recordModels")
    public List<RecordModel> records() {
        // get recordModels is this.Id
        if (recordModels == null || recordModels.isEmpty()) {
            recordModels = SQLite.select()
                    .from(RecordModel.class)
                    .where(RecordModel_Table.health_Id.is(id))
                    .and(RecordModel_Table.Status.isNot(STATUS_DELETE))
                    .queryList();
        }
        return recordModels;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }
}
