package net.qiujuer.tips.factory.view;

import net.qiujuer.tips.factory.model.db.ContactModel;
import net.qiujuer.tips.factory.model.db.HealthModel;
import net.qiujuer.tips.factory.util.TipsCalender;

public interface RecordAddView {

    String getHealthname();

    int getHealthvalue();

    int getHealthPerValue();

    int getType();

    String getBrief();

    int getColor();

    TipsCalender getDate();

    void setStatus(long status);

    ContactModel getContact();

    HealthModel getHealth();
}
