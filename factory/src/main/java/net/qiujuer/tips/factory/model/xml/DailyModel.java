package net.qiujuer.tips.factory.model.xml;


import android.content.SharedPreferences;

import net.qiujuer.tips.factory.util.TipsCalender;

public class DailyModel extends XmlPreference {
    private long DailyAddDate;
    private long ChangeDate;
    private int changevalue;
    private int monthsum;
    @Override
    protected void refresh(SharedPreferences sp) {
        DailyAddDate = sp.getLong("dailyadd_date", 201001010);//201001010

        TipsCalender calender = new TipsCalender(ChangeDate);
        if(DailyAddDate == 201001010){
            DailyAddDate = TipsCalender.getNow().toLong();
            save();
        }
        String monthdate = calender.toMonthDayString();
        monthsum  = sp.getInt(monthdate, 0);
    }

    @Override
    protected String getPreferenceName() {
        return DailyModel.class.getName();
    }

    @Override
    public void save() {
        SharedPreferences sp = getSharedPreferences();
        SharedPreferences.Editor editor = sp.edit();

        editor.putLong("dailyadd_date", DailyAddDate);
        TipsCalender calender = new TipsCalender(ChangeDate);
        String monthdate = calender.toSunYMenString();
    //    if (status == 0) {//add
            editor.putInt(monthdate, sp.getInt(monthdate, 0) + changevalue);
//        } else {//edit
//            editor.putInt(monthdate, sp.getInt(monthdate, 0) - mlastHealth + change);
//        }
        editor.apply();
        editor.commit();


    }

    public TipsCalender getDailyAddDate() {
        return new TipsCalender(DailyAddDate);
    }

    public void setDailyAddDate(TipsCalender date) {
        this.DailyAddDate = date.toLong();
    }

    public int getMonthsum() {
        return monthsum;
    }
    public int getMonthsumfromDate(String monthdate) {
        SharedPreferences sp = getSharedPreferences();
        int monthsum  = sp.getInt(monthdate, 0);
        return monthsum;
    }

    public void setMonthsum(int monthsum) {
        this.monthsum = monthsum;
    }

    public int getChangevalue() {
        return changevalue;
    }

    public void setChangevalue(int changevalue) {
        this.changevalue = changevalue;
    }

    public long getChangeDate() {
        return ChangeDate;
    }

    public void setChangeDate(long changeDate) {
        ChangeDate = changeDate;
    }


}
