/**
 * CREATE TABLE Contact
 * (
 * Id INTEGER PRIMARY KEY AUTOINCREMENT,
 * QQ TEXT,
 * Gender INTEGER,
 * Mark TEXT,
 * Name TEXT,
 * Phone TEXT,
 * Relation INTEGER,
 * Status INTEGER,
 * Changed INTEGER
 * )
 */
package net.qiujuer.tips.factory.model.db;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.qiujuer.tips.factory.model.adapter.ContactViewModel;

import java.util.List;
import java.util.UUID;

@ModelContainer
@Table(database = AppDatabase.class, name = "UserData")
public class UserDataModel extends BaseModel implements ModelStatus {

    public UserDataModel() {
        super();

    }

    @PrimaryKey(autoincrement = true)
    @Column(name = "Id")
    @Expose
    private long id;


    //月份统计
    @SerializedName("MonthDate")
    @Column(name = "MonthDate")
    private long MonthDate;


    @SerializedName("MonthSum")
    @Column(name = "MonthSum")
    private int MonthSum;


    @SerializedName("MonthReduce")
    @Column(name = "MonthReduce")
    private int MonthReduce;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMonthDate(long MonthDate) {
        this.MonthDate = MonthDate;
    }

    public long getMonthDate() {
        return MonthDate;
    }

    public void setMonthSum(int MonthSum) {
        this.MonthSum = MonthSum;
    }

    public int getMonthSum() {
        return MonthSum;
    }

    public void setMonthReduce(int MonthReduce) {
        this.MonthReduce = MonthReduce;
    }

    public int getMonthReduce() {
        return MonthReduce;
    }

    public static int getMonthValue(long date) {
        // Mark = ?
        UserDataModel Data = SQLite.select()
                .from(UserDataModel.class)
                .where(UserDataModel_Table.MonthDate.is(date))
                .querySingle();
        if(Data!=null){
            return Data.getMonthSum();
        }else{
            return 0;
        }

    }
    public static int getMonthReduceValue(long date) {
        // Mark = ?
        UserDataModel Data = SQLite.select()
                .from(UserDataModel.class)
                .where(UserDataModel_Table.MonthDate.is(date))
                .querySingle();
        if(Data!=null){
            return Data.getMonthReduce();
        }else{
            return 0;
        }
    }
/*
    public static long getLifeValue() {
        // Mark = ?  75年 197100 个 20分钟
        UserDataModel Data = SQLite.select()
                .from(UserDataModel.class)
                .where(UserDataModel_Table.MonthDate.is(date))
                .querySingle();
        return Data.getLifeSum();
    }
*/

}
