package net.qiujuer.tips.factory.presenter;

import net.qiujuer.tips.factory.cache.Cache;
import net.qiujuer.tips.factory.model.db.HealthModel;
import net.qiujuer.tips.factory.model.db.RecordModel;
import net.qiujuer.tips.factory.view.RecordAddView;

import java.text.SimpleDateFormat;
import java.util.UUID;


public class HealthAddPresenter {
    private RecordAddView mView;
    final SimpleDateFormat df = new SimpleDateFormat("yyyyMM");

    public HealthAddPresenter(RecordAddView view) {
        mView = view;
    }
//每刻钟 1 能量
    public static String[] BAD_TAG_NAME = {
            "晚睡",//10点以后 每刻钟 伤神 伤气 每一刻钟 消耗1 由于不睡 所以加倍消耗
            "手机电脑",//伤神
            "破色戒",//伤精人 年二十者，四日一泄；三十者，八日一泄；四十者，十六日一泄；五十者，二十日一泄；
                    // 六十者，闭精勿泄，若体力犹壮者，一月一泄。
            "醉酒",//伤神 一次 早晨起来 仍是昏沉 故一晚能量
            "过度劳累",//说话过多  运动多度  伤气
            "晚睡",
            "日常消耗"//根据不同工作 区分
    };
    public static float[] BAD_PER_VALUE = {
            2.0f,
            2.0f, //加倍消耗
            128.0f,//约等于4天的消耗 4*48 = 192
            8.0f,//醉酒 等于一晚没睡好 -8
            4.0f,//日常加倍
            4.0f,
            2.0f
    };
    public static String[] BAD_UNIT = {
            "刻钟(10点后)",//相对于10点以后 多一刻钟 多消耗
            "刻钟",
            "次",
            "小时",
            "次",
            "刻钟",
            "刻钟",
            "小时",
    };
    public static String[] GOOD_TAG_NAME = {
            "持心戒",//性功 功效 ★★★★★
            "持色戒",//命功 功效 ★★★★★
            "站桩",//命功 功效 ★★★★
            "打坐",//命功 功效 ★★★★
            "早睡",//命功 功效 ★★
            "卧功",//命功 功效 ★★
            "行功",//命功 功效 ★★
            "八段锦",//命功 功效 ★★
            "五禽戏",//命功 功效 ★★
            "马步冲拳",//命功 功效 ★★
            "大礼拜",//命功 功效 ★★
            "瑜伽",//命功 功效 ★★
            "过午不食",//命功 功效 ★
            "慢跑",//命功 功效 ★
    };
    public static float[] GOOD_PER_VALUE = {
            4.0f,
            8.0f,
            4.0f,
            4.0f,//完全入定=8.0f 相当于打坐一小时 深睡八小时 32能量 没有入定 眼耳鼻不耗散精气神 4.0f
            2.0f,
            4.0f,
            3.0f,
            4.0f,
            4.0f,
            4.0f,
            3.0f,//磕大头
            4.0f,//瑜伽
            6.0f,
            2.0f,//慢跑
    };
    public static String[] GOOD_UNIT = {
            "刻钟(120息)",
            "天",
            "刻钟",
            "刻钟",
            "刻钟(10点前)",
            "刻钟",
            "刻钟",
            "次",
            "次",
            "x150拳",
            "x15个",
            "次",
            "次",
            "刻钟",
    };

    public static void createHealthMode(String[] healthname,String[] healthpervalue,String[] healthunit
    ,String[] badname,String[] badpervalue,String[] badunit) {
        for (int i = 0; i < healthname.length; i++) {
            if (i  == healthname.length - 1 )
                create(healthname[i], healthpervalue[i], healthunit[i], 1, 48, 0, i);
            else
                create(healthname[i], healthpervalue[i], healthunit[i], 1, 0, 0, i);
        }
        for (int i = 0; i < badname.length; i++) {
            if(i == badname.length - 1 ){
                create(badname[i], badpervalue[i], badunit[i], 2, -80, 0, i);
            }else {
                create(badname[i], badpervalue[i], badunit[i], 2, 0, 0, i);
            }

        }
    }
    public static void editperoidvalue(UUID id, int peroidvalue) {
        HealthModel modeperoid = new HealthModel();
        HealthModel model = modeperoid.get(id);
        model.setPeriodvalue(peroidvalue);
        try {
            //clare 存储到数据库
            model.save();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void create(String name, String value, String unit, int type,
                              int peroidvalue, int quickvalue, int order) {
        HealthModel modelinstance = new HealthModel();
        HealthModel model = modelinstance.getfromorder(order,type);
        if(model == null) {//第一次初始化
            model = new HealthModel();
            model.setQuickvalue(quickvalue);
        }
        model.setMark(UUID.randomUUID());
        model.setName(name);
        model.setPerValue(Float.valueOf(value));
        model.setUnit(unit);
        model.setHealthtype(type);
        model.setPeriodvalue(peroidvalue);

        model.setOrder(order);
        try {
            //clare 存储到数据库
            model.save();
          //  Cache.getInstance().add(model);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
   /*　过去修炼的人，在这个阶段往往功力会大大增长。因为食物是属于阴性的东西，你吃得越多，越容易昏沉、
    打不起精神。也就是说你不吃食物，或者少吃食物的时候，体内的阳气相对来讲比较足。既然是阳气，它就往上升，
    而不是往下沉。
            　　所以过去很多开悟的人，都是在辟谷期间的凌晨开悟的。也就是两点以后，过了子时。
            因为这个时间大地的阳气是从下往上升的，我们体内的能量也是从下往上升的。道家和密宗讲的“开顶”，
            也大都是在这个时辰。其余的时间也会有，但是很少。因为其余的时间，体内的能量都是从上往下走。
            　　就像莲花开放的时候都是在清晨。早晨我们起床以后，发现莲花都开了！为什么中午以后，
            到了傍晚，莲花苞就都关闭了呢？因为午时一过，能量往下走了，莲花苞就关闭了。等到子时一过，能量又往上走了，莲花苞又全都打开了。
            　　我们人体生理的机能运转与自然界的植物是一模一样的。道教里面讲：“精满不思淫、
            气满不思食、神满不思睡”，这里所讲的“精”，是指人体的精力。我们通常讲的“某某人精力特别充沛”，就是指这个“精”。
            　　越是精力衰弱的人（或者讲身体虚弱的人），他的欲望越重。为什么呢？因为他的精力（或者说元气）
            不充足，体内就有足够的空间让它来流动，它就会乱串。而当一个人精力充沛的时候，它形成了一个
            整体的力量，越是充满越不容易走动，所以他的欲望很少产生。
    */
}
