package info.xinfu.carban;

import android.content.Context;
import android.os.Process;
import android.support.multidex.MultiDexApplication;

import cn.jpush.android.api.JPushInterface;
import info.xinfu.carban.BuildConfig;
import info.xinfu.carban.utils.JPushRegistTag;
import info.xinfu.carban.utils.SPUtils;
import com.hui.util.log.LogUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.cookie.CookieJarImpl;
import com.zhy.http.okhttp.cookie.store.PersistentCookieStore;
import com.zhy.http.okhttp.https.HttpsUtils;
import com.zhy.http.okhttp.log.LoggerInterceptor;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import okhttp3.Cache;
import okhttp3.OkHttpClient;


/**
 * @author Tianmu on 2016/10/8 0008 11:59
 * @email tianmu19@gmail.com
 */
public class MyApp extends MultiDexApplication {

    private static MyApp mInstance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //未捕获异常
        mInstance = this;
//        initJPush();

        initCrashReport();
//        Thread.setDefaultUncaughtExceptionHandler(this);

        //debug  open  ,realease close .
        LogUtils.initParam(BuildConfig.DEBUG);
//        LogUtils.initParam(false);
        //启动app设置默认api地址
        String baseurl = SPUtils.getString("app_baseurl", "");
        LogUtils.w(baseurl);
        initJPUSH();
        initRequestNetUtils();
        initLayout();

        initX5();
        initDirs();

    }

    private void initJPUSH() {
        JPushInterface.setDebugMode(BuildConfig.LOG_DEBUG);	// 设置开启日志,发布时请关闭日志
        JPushInterface.init(this); 		// 初始化 JPush
        JPushRegistTag.registerJPush();
    }

    private void initDirs() {
     /*   String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        String dir_yl = path + File.separator + Constants.YongLvDownload;
        String dir_fujian = dir_yl + File.separator + Constants.YongLvPSDownload;
        File file = new File(dir_yl);
        File file2 = new File(dir_fujian);
        if (!file.exists()) {
            file.mkdir();
        }
        if (!file2.exists()) {
            file2.mkdirs();
        }*/
    }



    private void initCrashReport() {
//        在开发测试阶段，可以在初始化Bugly之前通过以下接口把调试设备设置成“开发设备”。
        //    CrashReport.setIsDevelopmentDevice(getApplicationContext(), BuildConfig.DEBUG);
//        CrashReport.initCrashReport(getApplicationContext(), getResources().getString(R.string.bugly_crash), true);
    }

    private void initX5() {
        //AgentWeb
    }

    private void initLayout() {
      /*  LoadingLayout.getConfig()
                .setErrorText("出错啦，请稍后重试")
                .setEmptyText("抱歉，暂无数据")
                .setNoNetworkText(getResources().getString(R.string.net_error))
                .setErrorImage(R.mipmap.error_loading)
                .setEmptyImage(R.mipmap.empty_loading)
                .setNoNetworkImage(R.mipmap.nonet_loading)
                .setLoadingPageLayout(R.layout.loading_layout)
                .setAllTipTextColor(R.color.text_color_grey)
                .setAllTipTextSize(14)
                .setAllPageBackgroundColor(R.color.background_color)
                .setReloadButtonText("点我重试")
                .setReloadButtonTextSize(14)
                .setReloadButtonTextColor(R.color.text_color_grey)
                .setReloadButtonWidthAndHeight(140, 40);*/
    }

    private void initRequestNetUtils() {
        CookieJarImpl cookieJar = new CookieJarImpl(new PersistentCookieStore(getApplicationContext()));
        //设置可访问所有的https网站
        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new LoggerInterceptor("HttpsUtils"))
                .connectTimeout(15000L, TimeUnit.MILLISECONDS)
                .readTimeout(15000L, TimeUnit.MILLISECONDS)
                .writeTimeout(15000L, TimeUnit.MILLISECONDS)
                .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
                .cookieJar(cookieJar)
                .cache(new Cache(this.getCacheDir(), 3 * 1024 * 1024))
                .retryOnConnectionFailure(true)
                //其他配置
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

    public static MyApp getInstance() {
        return mInstance;
    }

    /**
     * 退出应用
     */
    public void exit() {
        try {
//            SDActivityManager.getInstance().finishAllActivity();
            System.exit(0);
            Process.killProcess(Process.myPid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

