package info.xinfu.carban.utils;

import android.view.Gravity;
import android.widget.Toast;

import info.xinfu.carban.MyApp;


/**
 * Created by tmgg on 2017/5/3.
 * tianmu19@gmail.com
 */

public class MyToastUtil {
    private static Toast t = null;

    private MyToastUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * App.getInstance()解决文字不居中问题
     *
     * @param msg 文字
     */
    public synchronized static void showCToast(Object msg) {
        if (t != null) {
            t.cancel();
        }
        t = Toast.makeText(MyApp.getInstance(), "" + msg, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }

    public synchronized static void showNToast(Object msg) {
        if (t != null) {
            t.cancel();
        }
        t = Toast.makeText(MyApp.getInstance(), "" + msg, Toast.LENGTH_SHORT);

        t.show();
    }
}
