package info.xinfu.carban.utils;

import android.os.Handler;
import android.text.TextUtils;

import com.hui.util.log.LogUtils;

import java.util.LinkedHashSet;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;
import info.xinfu.carban.MyApp;

/**
 * author: tmgg
 * created on: 2017/11/3 10:01
 * description: 极光推送注册绑定tag
 */
public class JPushRegistTag {
    /**
     * 删除tag
     */
    public static void unRegistTag() {
//        if(null!=mHandler)mHandler.removeCallbacksAndMessages(null);

        String userid = SPUtils.getString(Constants.username, "123");
        //正式
        String tag = "user_" + userid;
        // TODO: 2017/12/4  user_test_       测试版

        if (Constants.imgbase.contains(Constants.pot80)) {
            tag = "userT_" + userid;
        }

        // 检查 tag 的有效性
        if (TextUtils.isEmpty(tag)) {
            return;
        }

        // ","隔开的多个 转换成 Set
        String[] sArray = tag.split(",");
        Set<String> tagSet = new LinkedHashSet<String>();
        for (String sTagItme : sArray) {
            if (!ExampleUtil.isValidTagAndAlias(sTagItme)) {
                return;
            }
            tagSet.add(sTagItme);
        }
        mHandler.sendMessage(mHandler.obtainMessage(MSG_DELETE_TAGS, tagSet));
    }

    /**
     * 绑定
     */
    public static void registerJPush() {
        String userid = SPUtils.getString(Constants.username, "123");
        String tag = "user_" + userid;
        // TODO: 2017/12/4  user_test_       测试版

        if (Constants.imgbase.contains(Constants.pot80)) {
            tag = "userT_" + userid;
        }

        // 检查 tag 的有效性
        if (TextUtils.isEmpty(tag)) {
            return;
        }

        // ","隔开的多个 转换成 Set
        String[] sArray = tag.split(",");
        Set<String> tagSet = new LinkedHashSet<String>();
        for (String sTagItme : sArray) {
            if (!ExampleUtil.isValidTagAndAlias(sTagItme)) {
                return;
            }
            tagSet.add(sTagItme);
        }

        //调用JPush API设置Tag
        mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_TAGS, tagSet));
    }


    private static final int MSG_SET_TAGS = 1002;
    private static final int MSG_DELETE_TAGS = 1003;

    private static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
//                case MSG_SET_ALIAS:
//                    JPushInterface.setAliasAndTags(getApplicationContext(), (String) msg.obj, null, mAliasCallback);
//                    break;
                case MSG_SET_TAGS:
                    LogUtils.e("Set tags in handler.");
                    JPushInterface.setAliasAndTags(MyApp.getInstance(), null, (Set<String>) msg.obj, mTagsCallback);
                    break;

                case MSG_DELETE_TAGS:
                    LogUtils.e("delete msg - " + msg.what);
                    JPushInterface.deleteTags(MyApp.getInstance(), 0, (Set<String>) msg.obj);
            }
        }
    };


    private static TagAliasCallback mTagsCallback = new TagAliasCallback() {

        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
                    LogUtils.i(logs);
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    LogUtils.i(logs);
                    if (ExampleUtil.isConnected(MyApp.getInstance())) {
                        mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
                    } else {
                        LogUtils.i("No network");
                    }
                    break;
                default:
                    logs = "Failed with errorCode = " + code;
                    LogUtils.e(logs);
            }

        }

    };


}
