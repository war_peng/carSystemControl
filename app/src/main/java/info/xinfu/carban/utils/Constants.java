package info.xinfu.carban.utils;

/**
 * author: tmgg
 * created on: 2017/7/11 13:47
 * description:
 */

/**
 * Created by jingbin on 2016/11/26.
 * todo 参数
 */

public class Constants {
    public static final String initImgUrl = "initImgUrl";

    //用户id
    public static final String id = "id";
    // TODO: 2017/11/22 验证失败
    public static final String verificateFailed = "verificateFailed";
    public static final String updateurl = "http://114.215.105.133/mnt/taizhan/update.xml";
    public static final String updateapkurl = "http://114.215.105.133/mnt/taizhan/app-release.apk";
    public static final String pot80 = "8088";
    //--------------------------------------------------------------
    //正式
    public static String imgbase = "http://114.215.105.133:8880/nerborhood/";
    public static String username = "userId";
    public static String pwd = "pwd";
    public static String accessKey = "accessKey";
    public static String login = imgbase + "userLogin";
    public static String getmobilelist = imgbase + "getmobilelist";
    public static String getObserList = imgbase + "getobserverlist";
    public static String getsmssendlist = imgbase + "getsmssendlist";

    public static String addObserver = imgbase + "addObserver";
    public static String addUserMobile = imgbase + "addUserMobile";
    public static String deleteUserMobile = imgbase + "deleteUserMobile";
    public static String deleteObserver = imgbase + "deleteObserver";
    public static String getuserinfo = imgbase + "getuserinfo";
}
