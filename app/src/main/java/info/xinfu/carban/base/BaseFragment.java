package info.xinfu.carban.base;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import info.xinfu.carban.R;
import info.xinfu.carban.utils.ACache;
import info.xinfu.carban.utils.MyToastUtil;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import com.kaopiz.kprogresshud.KProgressHUD;

import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {
    protected Activity context;
    protected View rootView;
    private KProgressHUD hud1;
    private Unbinder unbinder;
    private Intent intentbase;
    public ACache mCache;
    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.context = getActivity();
        // to load the page (the LayoutInflater inflater passed on to the next
        // page)
        mCache = ACache.get(context);
        rootView = inflater.inflate(getRootViewId(),container,false);
        if (rootView == null)
            throw new NullPointerException("Fragment content view is null.");
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        requestServiceHook();
        initData();
        initListener();
    }
    @Override
    public void onResume() {
        super.onResume();
        resumeData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    /**
     * 绑定静态数据
     */
    protected void resumeData() {

    }

    protected abstract void initListener();

    /**
     * 配置一些网络参数
     */
    protected void requestServiceHook() {}


    protected abstract void initView();

    protected abstract void initData();
    protected abstract int getRootViewId();


    /**
     * show a toast
     */
    protected void showToast(String msg) {
        MyToastUtil.showNToast(msg);
    }
    /**
     * show a toast
     */
    protected void showCenterToast(String msg) {
        MyToastUtil.showCToast(msg);
    }

    /**
     * 进入下一个activity并且带一个动画效果
     */
//    protected final void enterNextActivity(Intent intent) {
//        ((Activity) context).startActivity(intent);
//        enterBeginAnimation();
//    }

    /**
     * 进入下一个activity并且带一个动画效果
     */
    protected final void startActivity(Class<?> clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
        enterBeginAnimation();
    }

    /**
     * 进入下一个activity的意图回调
     */
    public interface NextActivityIntentCallBack {
        void backIntent(Intent intent);
    }

    /**
     * 进入下一个activity并且带一个动画效果，向外界传一个回调(淡入淡出)
     */
//    protected final void enterNextActivity(Class<?> clazz,
//                                           NextActivityIntentCallBack intentCallBack) {
//        Intent intent = new Intent(context, clazz);
//        intentCallBack.backIntent(intent);
//        ((Activity) context).startActivity(intent);
////        ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
//    }
    /**
     * 进入下一个activity并且带一个动画效果(淡入淡出)
     */
    protected final void enterNextActivity(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(context, clazz);
        intent.putExtras(bundle);
        startActivity(intent);
        context.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }
    protected final void enterNextActivityTranAnim(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(context, clazz);
        intent.putExtras(bundle);
        startActivity(intent);
        enterBeginAnimation();
    }

    protected final void enterNextActivityTranAnim(Class<?> clazz, Intent intent) {
        startActivity(intent);
        enterBeginAnimation();
    }
    /**
     * To get to the next activity with an animation
     */
    protected void enterBeginAnimation() {
        context.overridePendingTransition(R.anim.tran_in,
                R.anim.tran_out);
    }


    /**
     * shake animation and show toast if prompt isn't empty
     *
     * @param view
     */
    protected void startShakeAnimationAndShowToast(View view, String prompt) {
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake_x);
        view.startAnimation(shake);
        if (!TextUtils.isEmpty(prompt)) {
            showToast(prompt);
        }
        view.requestFocus();
    }

    /**
     * shake animation and show toast if prompt isn't empty
     *
     */
    protected final boolean checkEditTextIsEmpty(EditText et, String prompt) {
        String etStr = et.getText().toString().trim();
        if (TextUtils.isEmpty(etStr)) {
            startShakeAnimationAndShowToast(et, prompt);
            return true;
        } else
            return false;
    }

    /**
     * 显示对话框，提示信息填写不完整，自定义文字
     */
    public void showIncompleteAlertDialog(Context context, String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true).setTitle("提示：").setMessage(content)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hidePDialog();

                    }
                }).show();
    }
    /**
     * 提交完成，退出当前活动
     */
    public void showFinishAlertDialog(final Context context, final String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false).setTitle("提示：").setMessage(content)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hidePDialog();

                        Activity activity = (Activity) context;
                        activity.finish();
                    }
                }).show();
    }



    /**
     * 判断字符串是否是字符串
     */
    public static boolean isGoodJson(String json) {

        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonParseException e) {
            System.out.println("bad json: " + json);
            return false;
        }
    }

    public void showPDialog() {
        try {
            hud1 = KProgressHUD.create(context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
            hud1.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hidePDialog() {
        if (hud1 !=null&& hud1.isShowing()) {
            try {
                hud1.dismiss();
                hud1 = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
