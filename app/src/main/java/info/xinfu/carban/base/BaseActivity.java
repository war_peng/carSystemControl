package info.xinfu.carban.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import info.xinfu.carban.R;
import info.xinfu.carban.utils.ACache;
import info.xinfu.carban.utils.CustomCrashHandler;
import info.xinfu.carban.utils.MyToastUtil;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import info.xinfu.carban.utils.StatusBarUtil;


/**
 * @author Tianmu
 * @email tianmu19@gmail.com
 * 继承权限管理
 */
public abstract class BaseActivity extends AppCompatActivity {
    private AlertDialog mLogindialog;
    private KProgressHUD hud1;
    protected Activity mContext;
    private Unbinder unbinder;
    private ACache mCache;
    private Intent intentbase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomCrashHandler.getInstance().setCustomCrashHanler(this);
        setRootView();
        StatusBarUtil.StatusBarLightMode(this);
        StatusBarUtil.setStatusBarColor(this,R.color.title_color);
        //在这里初始化，其他activity里使用直接报错，应该是找不到同一个上下文
        unbinder = ButterKnife.bind(this);
        mContext = this;
        mCache = ACache.get(mContext);
        initView();
        //网络请求的钩子
        requestServiceHook();
        initData();
        initListen();
    }

    public void showShakeEt(View view) {
        if (null == view) return;
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.shake);
        view.startAnimation(animation);
    }

    /**
     * 绑定静态数据
     */
    protected void resumeData() {

    }

    protected abstract void initView();


    protected abstract void setRootView();


    protected abstract void initData();

    protected abstract void initListen();

    /**
     * 访问网络数据的钩子
     */
    protected void requestServiceHook() {
    }

    /**
     * show string Toast
     */
    protected final void showToast(Object msg) {
        MyToastUtil.showNToast(msg);
    }

    protected final void showNetErrorToast() {
//        MyToastUtil.showNToast(getResources().getString(R.string.net_error));
    }

    protected final void showServerErrorToast() {
        MyToastUtil.showNToast("服务器出现异常！");
    }


    /**
     * 得到edit text中的文本
     *
     * @return
     */
//    protected final String getTextByEditText(EditText et) {
//        return et.getText().toString().trim();
//    }

    /**
     * 进入下一个activity并且带一个动画效果
     */
    public final void startActivity(Intent intent) {
        super.startActivity(intent);
        enterBeginAnimation();
    }

    /**
     * 进入下一个activity并且带一个动画效果
     */
    protected final void startActivity(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        super.startActivity(intent);
        enterBeginAnimation();
    }

    /**
     * 进入下一个activity并且带一个动画效果(淡入淡出)
     */
    protected final void enterNextActivity(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    protected final void enterNextActivityTranAnim(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(bundle);
        startActivity(intent);
        enterBeginAnimation();
    }

    /**
     * To get to the next activity with an animation
     */
    protected final void enterBeginAnimation() {
        overridePendingTransition(R.anim.tran_in, R.anim.tran_out);
    }

    protected final void backOutAnimation() {
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        //添加到管理器里
        resumeData();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        //结束activity

    }

    @Override
    public void finish() {
        //隐藏软键盘
//        GeneralUtil.hideSoftInputFromWindow(this);
        super.finish();
    }

    /**
     * 显示对话框，提示信息填写不完整，自定义文字
     */
    public void showIncompleteAlertDialog(Context context, String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true).setTitle("提示：").setMessage(content)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hidePDialog();

                    }
                }).show();
    }

    /**
     * 提交完成，退出当前活动
     */
    public void showFinishAlertDialog(final Context context, final String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false).setTitle("提示：").setMessage(content)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hidePDialog();

                        Activity activity = (Activity) context;
                        activity.finish();
                    }
                }).show();
    }


    /**
     * 判断字符串是否是字符串
     */
    public static boolean isGoodJson(String json) {

        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonParseException e) {
            System.out.println("bad json: " + json);
            return false;
        }
    }

    public void showPDialog() {
        if (hud1 != null && hud1.isShowing()) {
            return;
        }
        hud1 = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);

        hud1.show();
    }

    public void hidePDialog() {
        if (hud1 != null && hud1.isShowing()) {
            hud1.dismiss();
            hud1 = null;
        }

    }

    public ACache getAcache() {
        return mCache;
    }


    // 点击空白区域 自动隐藏软键盘
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (null != this.getCurrentFocus()) { /** * 点击空白位置 隐藏软键盘 */
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            return mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
        return super.onTouchEvent(event);
    }

    public void hideSoftInputManager() {
        if (null != this.getCurrentFocus()) {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }


}
