package info.xinfu.carban;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import info.xinfu.carban.utils.SPUtils;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;


public class ParseUrlService extends Service {

    protected static final String TAG = "ParseUrlService";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
//        startLocation();
        return Service.START_NOT_STICKY;
    }

    private void startLocation() {
        // 定位初始化
        new Thread(new Runnable() {
            @Override
            public void run() {

                // 网络加载HTML文档
                //"https://item.taobao.com/item.htm?spm=a1z38n.10678284.0.0.43861debhACZ7H&id=574743241203"
                String sellurl = SPUtils.getString("url", "");
                String shopname = "";
                String goodsname = "";

                // 网络加载HTML文档
                Document doc = null; // 使用GET方法访问URL
                try {
                    doc = Jsoup.connect(sellurl)
                            .timeout(5000) // 设置超时时间
                            .get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Element element = doc.selectFirst("div.tb-title");
                //  shopname = element.select("h3").text(); // 新闻标题
                goodsname = element.select("h3").attr("data-title"); // 新闻内容链接

                Element elementshopname = doc.selectFirst("div.tb-shop-name");
                shopname = elementshopname.select("a").attr("title");
                Element elementsoff = doc.selectFirst("div.J_TOffSale");
                EventBus.getDefault().post(new Event_Send_Message(true));
                String title = element.select("strong").text(); // 新闻标题
                String url = element.select("a").attr("href"); // 新闻内容链接
                Log.e("TAG", "Jsoup ======>>" + title + url);


            }
        }).start();


    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

}
