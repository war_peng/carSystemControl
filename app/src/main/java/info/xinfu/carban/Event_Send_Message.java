package info.xinfu.carban;

/**
 * author: tmgg
 * created on: 2017/7/22 18:21
 * description: 车禁进出入信息
 */
public class Event_Send_Message {
    private boolean isRefresh;

    public Event_Send_Message(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }

    public boolean isRefresh() {
        return isRefresh;
    }

    public void setRefresh(boolean refresh) {
        isRefresh = refresh;
    }
}
