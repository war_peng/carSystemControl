package info.xinfu.carban;

/**
 * author: tmgg
 * created on: 2017/7/22 18:21
 * description: 通知工作日志 刷新
 */
public class Event_Car_Info {
    private String msg;

    public Event_Car_Info(String message) {
        this.msg = message;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
