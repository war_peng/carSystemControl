package info.xinfu.carban.callback;

import com.sun.jna.Callback;

import info.xinfu.carban.data.PlateResult;

public interface IOnGetDataEx extends Callback
{
	public void callback(PlateResult.ByReference plateResult);
}

