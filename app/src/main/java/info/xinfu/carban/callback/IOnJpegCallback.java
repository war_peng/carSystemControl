package info.xinfu.carban.callback;

import com.sun.jna.Callback;

import info.xinfu.carban.data.DevData_info;

public interface IOnJpegCallback extends Callback
{
	public void callback(DevData_info.ByReference JpegInfo);
}
