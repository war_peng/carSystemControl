package info.xinfu.carban.callback;

import com.sun.jna.Callback;

import info.xinfu.carban.data.PlateResult_Ex;

public interface IOnGetDataEx2 extends Callback
{
	public void callback(PlateResult_Ex.ByReference plateResult);
}

