package info.xinfu.carban.entity;

import com.alibaba.fastjson.annotation.JSONField;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * @author clare
 */

public class RecordEntity  {

    //操作员

    private String operatorName = "1001";
    //车牌号

    private String cardNo = "";

    private long createTime;




    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardno) {
        this.cardNo = cardno;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createtime) {
        this.createTime = createtime;
    }


    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
