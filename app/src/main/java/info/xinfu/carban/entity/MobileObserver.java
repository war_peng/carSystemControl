package info.xinfu.carban.entity;

public class MobileObserver {

    /**
     * id : 1
     * userId : 1001
     * mobile : 13524774949
     * isDefault : null
     */

    private int id;
    private String userId;
    private String mobile;
    private int isDefault;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }
}
