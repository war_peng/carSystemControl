package info.xinfu.carban.entity;

public class SellerObserver {

    /**
     * observerId : 1
     * userId : 1001
     * postTime : 1533890690000
     * mobile : 13524774949
     * avatarUrl : https://item.taobao.com/item.htm?spm=a1z38n.10678284.0.0.43861debhACZ7H&id=574743241203
     * state : null
     * field1 : 某某旗舰店
     * field2 : 连衣裙
     * field3 : null
     * field4 : null
     * field5 : null
     * field6 : null
     * field7 : null
     * field8 : null
     * field9 : null
     * field10 : null
     * status : 1
     * createTime : null
     * updateTime : null
     */

    private int observerId;
    private String userId;
    private long postTime;
    private String mobile;
    private String avatarUrl;
    private Object state;
    private String field1;
    private String field2;
    private Object field3;
    private Object field4;
    private Object field5;
    private Object field6;
    private Object field7;
    private Object field8;
    private Object field9;
    private Object field10;
    private int status;
    private Object createTime;
    private Object updateTime;

    public int getObserverId() {
        return observerId;
    }

    public void setObserverId(int observerId) {
        this.observerId = observerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getPostTime() {
        return postTime;
    }

    public void setPostTime(long postTime) {
        this.postTime = postTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public Object getField3() {
        return field3;
    }

    public void setField3(Object field3) {
        this.field3 = field3;
    }

    public Object getField4() {
        return field4;
    }

    public void setField4(Object field4) {
        this.field4 = field4;
    }

    public Object getField5() {
        return field5;
    }

    public void setField5(Object field5) {
        this.field5 = field5;
    }

    public Object getField6() {
        return field6;
    }

    public void setField6(Object field6) {
        this.field6 = field6;
    }

    public Object getField7() {
        return field7;
    }

    public void setField7(Object field7) {
        this.field7 = field7;
    }

    public Object getField8() {
        return field8;
    }

    public void setField8(Object field8) {
        this.field8 = field8;
    }

    public Object getField9() {
        return field9;
    }

    public void setField9(Object field9) {
        this.field9 = field9;
    }

    public Object getField10() {
        return field10;
    }

    public void setField10(Object field10) {
        this.field10 = field10;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Object createTime) {
        this.createTime = createTime;
    }

    public Object getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Object updateTime) {
        this.updateTime = updateTime;
    }
}
