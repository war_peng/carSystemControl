package info.xinfu.carban.entity;

public class MessageObserver {

    /**
     * id : 56
     * messageType : 商品下架推送
     * title : 亮东方益盟专卖店
     * content : https://detail.tmall.com/item.htm?spm=a220z.1000880.0.0.F0lFQc&id=556730283099
     * filed1 : 1002
     * filed2 : null
     * filed3 : null
     * filed4 : null
     * filed5 : null
     * userId : 18676200530
     * createTime : 1534234694000
     */

    private int id;
    private String messageType;
    private String title;
    private String content;
    private String filed1;
    private Object filed2;
    private Object filed3;
    private Object filed4;
    private Object filed5;
    private String mobile;
    private long createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFiled1() {
        return filed1;
    }

    public void setFiled1(String filed1) {
        this.filed1 = filed1;
    }

    public Object getFiled2() {
        return filed2;
    }

    public void setFiled2(Object filed2) {
        this.filed2 = filed2;
    }

    public Object getFiled3() {
        return filed3;
    }

    public void setFiled3(Object filed3) {
        this.filed3 = filed3;
    }

    public Object getFiled4() {
        return filed4;
    }

    public void setFiled4(Object filed4) {
        this.filed4 = filed4;
    }

    public Object getFiled5() {
        return filed5;
    }

    public void setFiled5(Object filed5) {
        this.filed5 = filed5;
    }


    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
