package info.xinfu.carban.entity;

public class UserEntity {

    /**
     * id : 1001
     * groupId : 1
     * mobile : 13524774949
     * expiryDate : 1609430400000
     * totalMoney : 11
     * name : 战鹏
     * birthday : null
     * sex : false
     * crypt :
     * pwd : null
     * payPwd : null
     * avatar : null
     * isSmsVerify : false
     * provinceId : null
     * cityId : null
     * areaId : null
     * balance : 0.0
     * regTime : null
     * regIp : null
     * regProvinceId : null
     * regCityId : null
     * loginTime : null
     * loginIp : null
     * loginProvinceId : null
     * loginCityId : null
     * status : true
     * roomFeeLimitMax : null
     * totalIntegral : 0
     * integral : 0
     * invitationType : null
     * invitationId : null
     * openid : null
     * unionid : null
     * nameMatch : null
     */

    private int id;
    private int groupId;
    private String mobile;
    private long expiryDate;
    private int totalMoney;
    private String name;
    private Object birthday;
    private boolean sex;
    private String crypt;
    private Object pwd;
    private Object payPwd;
    private Object avatar;
    private boolean isSmsVerify;
    private Object provinceId;
    private Object cityId;
    private Object areaId;
    private double balance;
    private Object regTime;
    private Object regIp;
    private Object regProvinceId;
    private Object regCityId;
    private Object loginTime;
    private Object loginIp;
    private Object loginProvinceId;
    private Object loginCityId;
    private boolean status;
    private Object roomFeeLimitMax;
    private int totalIntegral;
    private int integral;
    private Object invitationType;
    private Object invitationId;
    private Object openid;
    private Object unionid;
    private Object nameMatch;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(int totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getBirthday() {
        return birthday;
    }

    public void setBirthday(Object birthday) {
        this.birthday = birthday;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getCrypt() {
        return crypt;
    }

    public void setCrypt(String crypt) {
        this.crypt = crypt;
    }

    public Object getPwd() {
        return pwd;
    }

    public void setPwd(Object pwd) {
        this.pwd = pwd;
    }

    public Object getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(Object payPwd) {
        this.payPwd = payPwd;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public boolean isIsSmsVerify() {
        return isSmsVerify;
    }

    public void setIsSmsVerify(boolean isSmsVerify) {
        this.isSmsVerify = isSmsVerify;
    }

    public Object getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Object provinceId) {
        this.provinceId = provinceId;
    }

    public Object getCityId() {
        return cityId;
    }

    public void setCityId(Object cityId) {
        this.cityId = cityId;
    }

    public Object getAreaId() {
        return areaId;
    }

    public void setAreaId(Object areaId) {
        this.areaId = areaId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Object getRegTime() {
        return regTime;
    }

    public void setRegTime(Object regTime) {
        this.regTime = regTime;
    }

    public Object getRegIp() {
        return regIp;
    }

    public void setRegIp(Object regIp) {
        this.regIp = regIp;
    }

    public Object getRegProvinceId() {
        return regProvinceId;
    }

    public void setRegProvinceId(Object regProvinceId) {
        this.regProvinceId = regProvinceId;
    }

    public Object getRegCityId() {
        return regCityId;
    }

    public void setRegCityId(Object regCityId) {
        this.regCityId = regCityId;
    }

    public Object getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Object loginTime) {
        this.loginTime = loginTime;
    }

    public Object getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(Object loginIp) {
        this.loginIp = loginIp;
    }

    public Object getLoginProvinceId() {
        return loginProvinceId;
    }

    public void setLoginProvinceId(Object loginProvinceId) {
        this.loginProvinceId = loginProvinceId;
    }

    public Object getLoginCityId() {
        return loginCityId;
    }

    public void setLoginCityId(Object loginCityId) {
        this.loginCityId = loginCityId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getRoomFeeLimitMax() {
        return roomFeeLimitMax;
    }

    public void setRoomFeeLimitMax(Object roomFeeLimitMax) {
        this.roomFeeLimitMax = roomFeeLimitMax;
    }

    public int getTotalIntegral() {
        return totalIntegral;
    }

    public void setTotalIntegral(int totalIntegral) {
        this.totalIntegral = totalIntegral;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public Object getInvitationType() {
        return invitationType;
    }

    public void setInvitationType(Object invitationType) {
        this.invitationType = invitationType;
    }

    public Object getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(Object invitationId) {
        this.invitationId = invitationId;
    }

    public Object getOpenid() {
        return openid;
    }

    public void setOpenid(Object openid) {
        this.openid = openid;
    }

    public Object getUnionid() {
        return unionid;
    }

    public void setUnionid(Object unionid) {
        this.unionid = unionid;
    }

    public Object getNameMatch() {
        return nameMatch;
    }

    public void setNameMatch(Object nameMatch) {
        this.nameMatch = nameMatch;
    }
}
