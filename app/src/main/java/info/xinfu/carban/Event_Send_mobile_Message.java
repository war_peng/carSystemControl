package info.xinfu.carban;

/**
 * author: tmgg
 * created on: 2017/7/22 18:21
 * description: 通知工作日志 刷新
 */
public class Event_Send_mobile_Message {
    private boolean isRefresh;

    public Event_Send_mobile_Message(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }

    public boolean isRefresh() {
        return isRefresh;
    }

    public void setRefresh(boolean refresh) {
        isRefresh = refresh;
    }
}
