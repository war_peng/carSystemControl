package info.xinfu.carban.event;

/**
 * Created by tmgg on 2016/12/13.
 * tianmu19@gmail.com
 * 登录的event
 */

public class Event_Play_Camera {
    private boolean refresh ;

    public Event_Play_Camera(boolean refresh) {
        this.refresh = refresh;
    }


    public boolean isRefresh() {
        return refresh;
    }

    public void setRefresh(boolean refresh) {
        this.refresh = refresh;
    }
}
