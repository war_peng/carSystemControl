package info.xinfu.carban.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.xinfu.carban.R;
import info.xinfu.carban.base.BaseActivity;
import info.xinfu.carban.fragment.BanFeeRootFragment;
import info.xinfu.carban.fragment.BanFeeRootFragment;
import info.xinfu.carban.fragment.RecordListFragment;
import info.xinfu.carban.fragment.SettingFragment;

/**
 * An ERP Base activity
 */
public class CarMainActivity extends BaseActivity {
    @BindView(R.id.home_replace_frameLayout)
    FrameLayout mFrameLayout;

    @BindView(R.id.home_tab_life_iv)
    ImageView homeTabLifeIv;
    @BindView(R.id.home_tab_life_tv)
    TextView homeTabLifeTv;
    @BindView(R.id.home_tab_life)
    RelativeLayout homeTabLife;
    @BindView(R.id.home_tab_chat_iv)
    ImageView homeTabChatIv;
    @BindView(R.id.home_tab_chat_tv)
    TextView homeTabChatTv;
    @BindView(R.id.home_tab_chat)
    RelativeLayout homeTabChat;
    @BindView(R.id.home_tab_mall_iv)
    ImageView homeTabMallIv;
    @BindView(R.id.home_tab_mall_tv)
    TextView homeTabMallTv;
    @BindView(R.id.home_tab_mall)
    RelativeLayout homeTabMall;
    @BindView(R.id.home_tab_my_iv)
    ImageView homeTabMyIv;
    @BindView(R.id.home_tab_my_tv)
    TextView homeTabMyTv;
    @BindView(R.id.home_tab_my)
    RelativeLayout homeTabMy;
    @BindView(R.id.tab)
    LinearLayout tab;

    private int selectIndex = 0;

    //当前的fragment是第几个
    private int currentIndex = 0;

 
    SettingFragment mSettingFragment;
    BanFeeRootFragment mFeeFragment;
    RecordListFragment mRecordFragment;
    @Override
    protected void initView() {
        //第一个页面tab点亮
        homeTabLifeIv.setImageResource(R.drawable.fee_press);
        homeTabLifeTv.setTextColor(getResources().getColor(R.color.theme_color));
        mFeeFragment = new BanFeeRootFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.home_replace_frameLayout, mFeeFragment)
                .show(mFeeFragment)
                .commit();
    }

    @Override
    protected void setRootView() {
        setContentView(R.layout.activity_car_main);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListen() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private void showFrameLayout() {
        mFrameLayout.setVisibility(View.VISIBLE);
    }
    @OnClick({R.id.home_tab_life, R.id.home_tab_chat, R.id.home_tab_mall, R.id.home_tab_my})
    public void onViewClicked(View view) {
        FragmentTransaction beginTransaction =
                getSupportFragmentManager().beginTransaction();
        final int COLOR_BLACK = getResources().getColor(R.color.text_color_black);
        final int COLOR_THEME = getResources().getColor(R.color.theme_color);
        switch (view.getId()) {
            case R.id.home_tab_life:
                hideFragments(beginTransaction);
                currentIndex = 1;
                restoreNormal();
                homeTabLifeIv.setImageResource(R.drawable.fee_press);
                homeTabLifeTv.setTextColor(COLOR_THEME);
                showFrameLayout();

                if (mFeeFragment == null) {
                    mFeeFragment = new BanFeeRootFragment();
                    beginTransaction.add(R.id.home_replace_frameLayout, mFeeFragment, "my").show(mFeeFragment);
                } else {
                    beginTransaction.show(mFeeFragment);
                }
                selectIndex = 1;
                break;
            case R.id.home_tab_chat:
                showToast("功能还未开放");
                /*
                hideFragments(beginTransaction);
                currentIndex = 2;
                restoreNormal();
                homeTabLifeIv.setImageResource(R.drawable.fee_press);
                homeTabLifeTv.setTextColor(COLOR_THEME);
                showFrameLayout();

                if (mFeeFragment == null) {
                    mFeeFragment = new BanFeeRootFragment();
                    beginTransaction.add(R.id.home_replace_frameLayout, mFeeFragment, "my").show(mFeeFragment);
                } else {
                    beginTransaction.show(mFeeFragment);
                }
                selectIndex = 2;*/
                break;
            case R.id.home_tab_mall:
                hideFragments(beginTransaction);
                currentIndex = 3;
                restoreNormal();
                homeTabMallIv.setImageResource(R.drawable.fee_press);
                homeTabMallTv.setTextColor(COLOR_THEME);
                showFrameLayout();

                if (mRecordFragment == null) {
                    mRecordFragment = new RecordListFragment();
                    beginTransaction.add(R.id.home_replace_frameLayout, mRecordFragment, "record").show(mRecordFragment);
                } else {
                    beginTransaction.show(mRecordFragment);
                }
                selectIndex = 3;
                break;
            case R.id.home_tab_my:
                hideFragments(beginTransaction);
                currentIndex = 4;
                restoreNormal();
                homeTabMyIv.setImageResource(R.drawable.setting_press);
                homeTabMyTv.setTextColor(COLOR_THEME);
                showFrameLayout();

                if (mSettingFragment == null) {
                    mSettingFragment = new SettingFragment();
                    beginTransaction.add(R.id.home_replace_frameLayout, mSettingFragment, "setting").show(mSettingFragment);
                } else {
                    beginTransaction.show(mSettingFragment);
                }
                selectIndex = 4;
                break;
            default:
                break;
        }
        beginTransaction.commit();
    }

    /**
     * 恢复按钮和字体颜色
     */
    private void restoreNormal() {
        homeTabLifeIv.setImageResource(R.drawable.fee_normal);
        homeTabChatIv.setImageResource(R.drawable.duty_normal);
        homeTabMallIv.setImageResource(R.drawable.record_normal);
        homeTabMyIv.setImageResource(R.drawable.setting_normal);


        homeTabLifeTv.setTextColor(getResources().getColor(R.color.tabcolor_normal));
        homeTabChatTv.setTextColor(getResources().getColor(R.color.tabcolor_normal));
        homeTabMallTv.setTextColor(getResources().getColor(R.color.tabcolor_normal));
        homeTabMyTv.setTextColor(getResources().getColor(R.color.tabcolor_normal));

    }


    /**
     * 将所有的Fragment都置为隐藏状态。
     *
     * @param transaction 用于对Fragment执行操作的事务
     */
    @SuppressLint("NewApi")
    private void hideFragments(FragmentTransaction transaction) {



        if (mFeeFragment != null) {
            transaction.hide(mFeeFragment);
        }

    /*    if (mFeeFragment != null) {
            transaction.hide(mFeeFragment);
        }*/
        if (mSettingFragment != null) {
            transaction.hide(mSettingFragment);
        }
        if (mRecordFragment != null) {
            transaction.hide(mRecordFragment);
        }
      /*  if (mYiLife != null) {
            transaction.hide(mYiLife);
        }
        if (mNeighborCircle != null) {
            transaction.hide(mNeighborCircle);
        }
        if (groupFragment != null) {
            transaction.hide(groupFragment);
        }
        if (mChangeMall != null) {
            transaction.hide(mChangeMall);
        }*/

    }
}
