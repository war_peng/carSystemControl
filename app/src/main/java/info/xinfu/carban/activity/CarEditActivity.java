package info.xinfu.carban.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.xinfu.carban.R;
import info.xinfu.carban.base.BaseActivity;

/**
 * @author clare
 */
public class CarEditActivity extends BaseActivity {


    @BindView(R.id.id_include_head_title)
    TextView idIncludeHeadTitle;
    @BindView(R.id.id_include_head_goback)
    RelativeLayout idIncludeHeadGoback;
    @BindView(R.id.id_include_head_right)
    TextView idIncludeHeadRight;
    @BindView(R.id.car_type)
    TextView carType;
    @BindView(R.id.tv_entry_time)
    TextView tvEntryTime;
    @BindView(R.id.tv_exit_time)
    TextView tvExitTime;
    @BindView(R.id.tv_park_time)
    TextView tvParkTime;
    @BindView(R.id.tv_fee_value)
    TextView tvFeeValue;
    @BindView(R.id.tv_charged_value)
    TextView tvChargedValue;
    @BindView(R.id.tv_reduce_value)
    TextView tvReduceValue;
    @BindView(R.id.tv_should_value)
    TextView tvShouldValue;
    @BindView(R.id.tv_sum_value)
    TextView tvSumValue;
    @BindView(R.id.btn_start_play)
    Button btnStartPlay;

    @Override
    protected void initView() {
        idIncludeHeadTitle.setText("编辑");
    }

    @Override
    protected void setRootView() {
        setContentView(R.layout.activity_car_edit);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListen() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.id_include_head_goback)
    public void onViewClicked() {
        this.finish();
    }
}
