package info.xinfu.carban.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import info.xinfu.carban.Event_Send_Message;
import info.xinfu.carban.R;
import info.xinfu.carban.base.BaseFragment;
import info.xinfu.carban.utils.SPUtils;
import com.vondear.rxtools.RxTimeUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static org.greenrobot.eventbus.EventBus.getDefault;

public class SellerObserLogFragment extends BaseFragment {
    //存放请假类型的map
    protected float[] mValues = new float[]{25.14f, 74.86f};
    protected float[] mValues2 = new float[]{35.20f, 64.8f};
    @BindView(R.id.tv_log_info)
    TextView tvLogInfo;
    Unbinder unbinder;
    @BindView(R.id.tv_log_time)
    TextView tvLogTime;
    private int mYear;
    private boolean isFirstEntry = false;

    private String mDate = "";
    //当前页码
    private int pageNo = 1;
    private boolean isLastPage = false;


    public static SellerObserLogFragment getInstance(String type) {
        SellerObserLogFragment fragment1 = new SellerObserLogFragment();
        Bundle bdl = new Bundle();
        bdl.putString("type", type);
        fragment1.setArguments(bdl);
        return fragment1;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventRefresh(Event_Send_Message event) {
        if (event.isRefresh()) {
            tvLogTime.setText(RxTimeUtils.getCurTimeString());
            tvLogInfo.setText("监控到链接地址为:" + SPUtils.getString("url", "") + "宝贝下架，邮件已发送至" +
                    SPUtils.getString("email", ""));
        }
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initView() {
        getDefault().register(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getRootViewId() {
        return R.layout.fragment_seller_log;
    }


    @OnClick({R.id.btn_bottom})
    void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_bottom:
                tvLogInfo.setText("");
                tvLogTime.setText("");
            default:

                break;
        }
    }
    private void parseJsonString(String response) {

        JSONObject jsonObject = JSON.parseObject(response);
        String resCode = jsonObject.getString("resCode");
        String resMsg = jsonObject.getString("resMsg");

    }

    private void getData(int year, int month) {
      /*  final String uname = SPUtils.getString(Constants.username, "");

        if (RxNetUtils.isAvailable(context)) {
            LogUtils.w("pageNo: " + pageNo);
            String akey = SPUtils.getString(Constants.accessKey, "");
            OkHttpUtils.post().url(Constants.one_month_journal)
                    .addParams(Constants.username, uname)
                    .addParams(Constants.accessKey, akey)
                    .addParams("month", String.valueOf(month))
                    .addParams("year", String.valueOf(year))
                    .build().execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    LogUtils.e(e.getMessage());

                }

                @Override
                public void onResponse(String response, int id) {
                    LogUtils.w(response);

                    if (!TextUtils.isEmpty(response) && isGoodJson(response)) {
                        parseJsonString(response);
                    } else {

                    }

                }
            });
        } else {

        }*/

    }

    private void setData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
