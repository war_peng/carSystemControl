package info.xinfu.carban.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import info.xinfu.carban.R;
import info.xinfu.carban.base.BaseFragment;
import info.xinfu.carban.utils.Constants;
import info.xinfu.carban.utils.SPUtils;

public class SettingFragment extends BaseFragment {
    //存放请假类型的map
    protected float[] mValues = new float[]{25.14f, 74.86f};
    protected float[] mValues2 = new float[]{35.20f, 64.8f};
    @BindView(R.id.id_include_head_title)
    TextView idIncludeHeadTitle;
    @BindView(R.id.id_include_head_goback)
    RelativeLayout idIncludeHeadGoback;
    @BindView(R.id.id_include_head_right)
    TextView idIncludeHeadRight;
    @BindView(R.id.ll_kefu)
    LinearLayout llKefu;
    @BindView(R.id.ll_developed_by)
    LinearLayout llDevelopedBy;
    Unbinder unbinder;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    private int mYear;
    private boolean isFirstEntry = false;

    private String mDate = "";
    //当前页码
    private int pageNo = 1;
    private boolean isLastPage = false;


    public static SettingFragment getInstance(String type) {
        SettingFragment fragment1 = new SettingFragment();
        Bundle bdl = new Bundle();
        bdl.putString("type", type);
        fragment1.setArguments(bdl);
        return fragment1;
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initView() {
        idIncludeHeadGoback.setVisibility(View.GONE);
        idIncludeHeadTitle.setText("设置");
        tvAccount.setText(SPUtils.getString(Constants.username,""));
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getRootViewId() {
        return R.layout.fragment_setting;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
