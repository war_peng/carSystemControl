package info.xinfu.carban.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import info.xinfu.carban.R;
import info.xinfu.carban.entity.SellerObserver;

import java.util.List;

/**
 * author: tmgg
 * created on: 2017/12/4 16:46
 * description:论坛适配器
 */
public class ObserverListAdapter extends BaseQuickAdapter<SellerObserver, BaseViewHolder> {


    public ObserverListAdapter(int layoutResId, @Nullable List<SellerObserver> data) {
        super(layoutResId, data);

    }

    @Override
    protected void convert(BaseViewHolder helper, final SellerObserver bean) {
        Context context = helper.getConvertView().getContext();

        int color_red = context.getResources().getColor(R.color.text_color_red);
        int color_green = context.getResources().getColor(R.color.text_color_green);
        helper.setText(R.id.fee_good,bean.getField2());
        helper.setText(R.id.fee_url,bean.getAvatarUrl());
        helper.setText(R.id.fee_shop,bean.getField1());
        if(bean.getStatus() == 0){
            helper.setText(R.id.fee_status,"已下架");
            helper.setTextColor(R.id.fee_status,color_red);
        } else {
            helper.setText(R.id.fee_status,"上架-监控中");
            helper.setTextColor(R.id.fee_status,color_green);
        }
        helper.addOnClickListener(R.id.delete);
    }

}
