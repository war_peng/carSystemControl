package info.xinfu.carban.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import info.xinfu.carban.R;
import info.xinfu.carban.entity.RecordEntity;
import info.xinfu.carban.utils.TimeUtil;

/**
 * author: tmgg
 * created on: 2017/12/4 16:46
 * description:论坛适配器
 */
public class RecordListAdapter extends BaseQuickAdapter<RecordEntity, BaseViewHolder> {


    public RecordListAdapter(int layoutResId, @Nullable List<RecordEntity> data) {
        super(layoutResId, data);

    }

    @Override
    protected void convert(BaseViewHolder helper, final RecordEntity bean) {
        Context context = helper.getConvertView().getContext();

        int color_red = context.getResources().getColor(R.color.text_color_red);
        int color_green = context.getResources().getColor(R.color.text_color_green);
        helper.setText(R.id.send_date, TimeUtil.getTimeWithLong(bean.getCreateTime()));
        helper.setText(R.id.fee_url,bean.getOperatorName());
        helper.setText(R.id.send_mobile,bean.getCardNo());
//        helper.addOnClickListener(R.id.delete);
    }

}
