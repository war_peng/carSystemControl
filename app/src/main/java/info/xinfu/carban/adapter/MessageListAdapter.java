package info.xinfu.carban.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import info.xinfu.carban.R;
import info.xinfu.carban.entity.MessageObserver;
import info.xinfu.carban.utils.TimeUtil;

import java.util.List;

/**
 * author: tmgg
 * created on: 2017/12/4 16:46
 * description:论坛适配器
 */
public class MessageListAdapter extends BaseQuickAdapter<MessageObserver, BaseViewHolder> {


    public MessageListAdapter(int layoutResId, @Nullable List<MessageObserver> data) {
        super(layoutResId, data);

    }

    @Override
    protected void convert(BaseViewHolder helper, final MessageObserver bean) {
        Context context = helper.getConvertView().getContext();

        int color_red = context.getResources().getColor(R.color.text_color_red);
        int color_green = context.getResources().getColor(R.color.text_color_green);
        helper.setText(R.id.send_date, TimeUtil.getTimeWithLong(bean.getCreateTime()));
        helper.setText(R.id.fee_url,bean.getContent());
        helper.setText(R.id.send_mobile,bean.getMobile());
//        helper.addOnClickListener(R.id.delete);
    }

}
