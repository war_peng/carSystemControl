package info.xinfu.carban.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import info.xinfu.carban.R;
import info.xinfu.carban.entity.MobileObserver;

import java.util.List;

/**
 * author: tmgg
 * created on: 2017/12/4 16:46
 * description:论坛适配器
 */
public class MobileListAdapter extends BaseQuickAdapter<MobileObserver, BaseViewHolder> {


    public MobileListAdapter(int layoutResId, @Nullable List<MobileObserver> data) {
        super(layoutResId, data);

    }

    @Override
    protected void convert(BaseViewHolder helper, final MobileObserver bean) {
        helper.setText(R.id.tv_type_name,"手机号" + helper.getAdapterPosition());
        helper.setText(R.id.fee_mobile,bean.getMobile());
        helper.addOnClickListener(R.id.delete);
    }

}
