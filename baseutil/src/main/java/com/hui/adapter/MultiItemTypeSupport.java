package com.hui.adapter;
/**
 * 支持多布局接口
 */
public interface MultiItemTypeSupport<T> {
	int getLayoutId(int position, T item);
}
