package com.hui.util.image;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hui.util.GeneralUtil;
import com.hui.util.image.glide.GlideCircleTransform;

import java.io.File;

/**
 * ============================================================
 * <p/>
 * copyright ZENG　HUI (c) 2014
 * <p/>
 * author : HUI
 * <p/>
 * version : 1.0
 * <p/>
 * date created : On November 24, 2014 9:14:37
 * <p/>
 * description : 图片加载的耦合工具类
 * <p/>
 * revision history :
 * <p/>
 * ============================================================
 */
public class ImageLoadCouplingUtil {
    private static ImageLoadCouplingUtil mInstance;

    private static int mErrorResouce;

    public static void initErrorResouce(int errorResouce) {
        mErrorResouce = errorResouce;
    }

    private ImageLoadCouplingUtil() {

    }

    public static ImageLoadCouplingUtil getInstance() {
        if (mInstance == null) {
            synchronized (ImageLoadCouplingUtil.class) {
                if (mInstance == null) {
                    mInstance = new ImageLoadCouplingUtil();
                }
            }
        }
        return mInstance;
    }

    /**************************
     * 本地文件圆形图片加载
     ****************************/
    public void loadCircleImage(File file, ImageView imageView) {
        Glide.with(imageView.getContext()).load(file)
                .transform(new GlideCircleTransform(imageView.getContext()))
                .error(mErrorResouce).into(imageView);
    }

    public void loadCircleImage(File file, ImageView imageView, int errorResouce) {
        Glide.with(imageView.getContext()).load(file)
                .transform(new GlideCircleTransform(imageView.getContext()))
                .error(errorResouce).into(imageView);
    }

    /**************************
     * 本地资源图片加载
     ****************************/
    public void loadCircleImage(int resouceId, ImageView imageView) {
        Glide.with(imageView.getContext()).load(resouceId)
                .transform(new GlideCircleTransform(imageView.getContext()))
                .error(mErrorResouce).into(imageView);
    }

    public void loadCircleImage(int resouceId, ImageView imageView,
                                int errorResouce) {
        Glide.with(imageView.getContext()).load(resouceId)
                .transform(new GlideCircleTransform(imageView.getContext()))
                .error(errorResouce).into(imageView);
    }

    /**************************
     * url圆形图片加载
     ****************************/
    public void loadCircleImage(String url, ImageView imageView) {
        displayCircleImage(url, imageView, mErrorResouce);
    }

    public void loadCircleImage(String url, ImageView imageView,
                                int errorResouce) {
        displayCircleImage(url, imageView, errorResouce);
    }

    private void displayCircleImage(String url, ImageView imageView,
                                    int errorResouce) {
        // 如果是一个合法的网络路径认为它是这个
        Glide.with(imageView.getContext())
                .load(url)
                .transform(new GlideCircleTransform(imageView.getContext()))
                .error(errorResouce).into(imageView);


    }

    /**************************
     * 本地文件图片加载
     ****************************/
    public void loadImage(File file, ImageView imageView) {
        Glide.with(imageView.getContext()).load(file).error(mErrorResouce)
                .into(imageView);
    }

    public void loadImage(File file, ImageView imageView, int errorResouce) {
        Glide.with(imageView.getContext()).load(file).error(errorResouce)
                .into(imageView);
    }

    /**************************
     * 本地资源图片加载
     ****************************/
    public void loadImage(int resouceId, ImageView imageView) {
        Glide.with(imageView.getContext()).load(resouceId).error(mErrorResouce)
                .into(imageView);
    }

    public void loadImage(int resouceId, ImageView imageView, int errorResouce) {
        Glide.with(imageView.getContext()).load(resouceId).error(errorResouce)
                .into(imageView);
    }

    /**************************
     * url图片加载
     ****************************/
    public void loadImage(String url, ImageView imageView) {
        display(url, imageView, mErrorResouce);
    }

    public void loadImage(String url, ImageView imageView, int errorResouce) {
        display(url, imageView, errorResouce);
    }

    private void display(String url, ImageView imageView, int errorResouce) {
        Glide.with(imageView.getContext()).load(url).error(errorResouce)
                .into(imageView);
    }
}
