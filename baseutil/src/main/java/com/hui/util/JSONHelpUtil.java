package com.hui.util;

import android.annotation.SuppressLint;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Type;

public class JSONHelpUtil {
	
	public static String toJSONFromGeneric(Object object) {
		Type mySuperClass = object.getClass().getGenericSuperclass();
		Gson gson = new GsonBuilder().create();
		return gson.toJson(object, mySuperClass);
	}
	/**
	 * An object into a string
	 * @param object
	 * @return
	 */
	public static String toJSON(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}

	/**
	 * Put the string into a generic objects
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> T parseObject(String json, Type type) {
		Gson gson = new Gson();
		return gson.fromJson(json, type);
	}
	
	/**
	 * @param fileName
	 * @return
	 */
	@SuppressLint("NewApi")
	@SuppressWarnings("resource")
	public static String FileToBase64(String fileName) {
		String data = "";
		try {
			File file = new File(fileName);
			FileInputStream in = new FileInputStream(file);
			byte[] buffer = new byte[(int) file.length() + 100];
			int length = in.read(buffer);
			data = Base64.encodeToString(buffer, 0, length, Base64.DEFAULT);
		} catch (Exception ex) {
		}
		return data;
	}
}
