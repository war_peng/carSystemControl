package com.hui.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.TrafficStats;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Field;

public class AppUtil {

	/**
	 * 获取当前应用的包名
	 */
	public static String getApplicationName(Context context) {
		PackageManager packageManager = null;
		ApplicationInfo applicationInfo = null;
		try {
			packageManager = context.getApplicationContext()
					.getPackageManager();
			applicationInfo = packageManager.getApplicationInfo(
					context.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			applicationInfo = null;
		}
		String applicationName = (String) packageManager
				.getApplicationLabel(applicationInfo);
		return applicationName;
	}
	
	/**
	 * Get traffic data
	 * @return
	 */
	@TargetApi(Build.VERSION_CODES.FROYO)
	@SuppressLint("NewApi")
	public static String getTrafficInfo()
	{
		long receive= TrafficStats.getTotalRxBytes()/1024;
		long send=TrafficStats.getTotalTxBytes()/1024;
		return "接受"+receive+"kb \n"+"发送"+send+"kb \n"+"总共"+(receive+send)+"kb";
	}
	
	/**
	 * Access to the phone connection
	 * @return
	 */
	public static String getBrandModel()
	{
		String brandmodel= Build.BRAND+" "+ Build.MODEL;
		if(brandmodel.trim().equals(""))
			brandmodel="unkown";
		return brandmodel;
	}
	
	/**
	 * Cell phone information
	 * @return
	 */
	public static String getMobileInfo() {
		StringBuffer sb = new StringBuffer();
		try {

			Field[] fields = Build.class.getDeclaredFields();
			for(Field field: fields){
				field.setAccessible(true);
				String name = field.getName();
				String value = field.get(null).toString();
				sb.append(name+"="+value);
				sb.append("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	/**
	 * 返回当前程序版本名
	 */
	public static String getAppVersionName(Context context) {
		String versionName = "";
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			versionName = pi.versionName;
			if (versionName == null || versionName.length() <= 0) {
				return "";
			}
		} catch (Exception e) {
			Log.e("VersionInfo", "Exception", e);
		}
		return versionName;
	}
	
	/**
	 * 获取设备id
	 */
	public final static String getDeviceId(Context context){
		TelephonyManager tm = (TelephonyManager)context.
				getSystemService(Context.TELEPHONY_SERVICE); 
		return tm.getDeviceId(); 
	}
	
	/**
	 * @param get
	 *            version code (Internal identification number)
	 * 
	 * @return
	 */
	public static int getVersionCode(Context context) {
		try {
			PackageInfo pi = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return pi.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return 0;
		}
	}
}
