package com.hui.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

public class SquareRoundImageView extends ImageView {
	private Paint mPaint;

	public SquareRoundImageView(Context context) {
		this(context,null);
	}

	public SquareRoundImageView(Context context, AttributeSet attrs) {
		this(context, attrs,0);
	}

	public SquareRoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mPaint  = new Paint();
	}

	@Override
	public void setImageBitmap(Bitmap bm) {
		// 将Bitmap转为正方形的drawable
		SquareImageDrawable drawable = new SquareImageDrawable(bm);
		setImageDrawable(drawable);
	}

	@Override
	public void setBackground(Drawable background) {
		// 如果想在布局xml代码中设置方形背景图片，可以在这里做处理，可以先将drawable转为bitmap，但是也没必要处理，直接放一个圆形的背景图片即可
		super.setBackground(background);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		setMeasuredDimension(widthSize, widthSize);
	}

	public void setImageUrl(String url){
		Glide.with(getContext()).load(url).asBitmap().into(new SimpleTarget<Bitmap>() {
			@Override
			public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
				setImageBitmap(resource);
			}
		});
	}

	/**
	 * 绘制圆角矩形图片
	 * @author caizhiming
	 */
	@Override
	protected void onDraw(Canvas canvas) {

		Drawable drawable = getDrawable();
		if (null != drawable) {
			Bitmap bitmap = ((SquareImageDrawable) drawable).getBitmap();
			Bitmap b = getRoundBitmap(bitmap, 20);
			final Rect rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());
			final Rect rectDest = new Rect(0,0,getWidth(),getHeight());
			mPaint.reset();
			canvas.drawBitmap(b, rectSrc, rectDest, mPaint);

		} else {
			super.onDraw(canvas);
		}
	}

	/**
	 * 获取圆角矩形图片方法
	 * @param bitmap
	 * @param roundPx,一般设置成14
	 * @return Bitmap
	 * @author caizhiming
	 */
	private Bitmap getRoundBitmap(Bitmap bitmap, int roundPx) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;

		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		mPaint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		mPaint.setColor(color);
		int x = bitmap.getWidth();

		canvas.drawRoundRect(rectF, roundPx, roundPx, mPaint);
		mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, mPaint);
		return output;


	}


	public class SquareImageDrawable extends Drawable {

		private Paint mPaint;
		private int mWidth;
		private Bitmap mBitmap;

		public SquareImageDrawable(Bitmap bitmap) {
			mBitmap = bitmap;
			BitmapShader bitmapShader = new BitmapShader(bitmap,
					TileMode.CLAMP, TileMode.CLAMP);
			mPaint = new Paint();
			mPaint.setAntiAlias(true);
			mPaint.setShader(bitmapShader);
			mWidth = Math.min(mBitmap.getWidth(), mBitmap.getHeight());
		}

		@Override
		public void draw(Canvas canvas) {
			canvas.drawRect(0, 0, mWidth, mWidth, mPaint);
		}

		@Override
		public int getIntrinsicWidth() {
			return mWidth;
		}

		@Override
		public int getIntrinsicHeight() {
			return mWidth;
		}

		@Override
		public void setAlpha(int alpha) {
			mPaint.setAlpha(alpha);
		}

		@Override
		public void setColorFilter(ColorFilter cf) {
			mPaint.setColorFilter(cf);
		}

		@Override
		public int getOpacity() {
			return PixelFormat.TRANSLUCENT;
		}

		public Bitmap getBitmap(){
			return mBitmap;
		}

	}
}
